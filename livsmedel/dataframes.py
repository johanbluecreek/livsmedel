import datetime
import pandas as pd
import numpy as np
from fuzzywuzzy import fuzz

class SearchableDataFrame(pd.DataFrame):

    def fuzzy_search(self, column, phrase, num=10, verbose=False, restrict_col=None, restrict_val=None):
        """
        fuzzy_search(column, phrase, num=10)

        Searches column with name `column` in the dataframe `df` for
        phrase `phrase` and returns the `num` best matches. Returns
        a copied dataframe with those entries.
        """
        if column is None or phrase is None:
            return None
        working_df = self.copy()
        if restrict_col is not None and restrict_val is not None:
            working_df = working_df[working_df[restrict_col] == restrict_val]
        try:
            fuzz_ratio_series = working_df[column].map(lambda name: fuzz.ratio(name, phrase))
        except KeyError:
            # if we misspelled the column name, we guess it and warn
            new_col = (
                lambda series:
                    series.iloc[
                        series.map(
                            lambda name: fuzz.ratio(name, column)
                        ).sort_values(ascending=False)[:1].index
                    ]
                )(pd.Series(working_df.keys())).values[0]
            verbose and print(f"Key '{column}' not present. Assuming you meant '{new_col}'.")
            fuzz_ratio_series = working_df[new_col].map(lambda name: fuzz.ratio(name, phrase))
        fuzz_ratio_series = fuzz_ratio_series.sort_values(ascending=False)[:num]
        return working_df.loc[fuzz_ratio_series.index]

    def _is_integer(self, reply):
        try:
            if int(reply) - float(reply) == 0:
                return True
            return False
        except ValueError:
            return False
        return False

    def _make_nan_safe(self, val):
        if isinstance(val, float):
            if np.isnan(val):
                return 'NaN'
        return val

    def interactively_select(self, column='Namn', exit_prompt='"q" för att återvända', extra_prompt=None, restrict_col=None, restrict_val=None, full_table=False, verbose=False):
        if full_table:
            current_frame = self
            print(current_frame)
        else:
            current_frame = self.fuzzy_search(None, None)
        while True:
            prompt = 'Sökord [' + exit_prompt + '] {q}: ' if (current_frame is None or extra_prompt is None) else f'Sökord [{extra_prompt}, '+ exit_prompt +'] {q}: '
            reply = input(prompt)
            if reply == 'q' or reply == '':
                return None
            if self._is_integer(reply):
                if current_frame is not None:
                    reply = int(reply)
                    if reply in current_frame['Nummer'].values:
                        return reply
                    else:
                        print('Nummer ej i sökresultat. Försök igen.')
                        continue
            current_frame = self.fuzzy_search(column, reply, verbose=verbose, restrict_col=restrict_col, restrict_val=restrict_val)
            print(current_frame)

class VictualDataFrame(SearchableDataFrame):

    def _interactively_get_new_victual_row(self, name=None):
        number = self['Nummer'].max()
        number = 1 if np.isnan(number) else number + 1

        if name is None:
            name = input('Ange namnet på livsmedlet som ska läggas till: ')

        return {
            'Nummer': number,
            'Namn': name,
            'ViktGram': 100.0,
            'Huvudgrupp': 'Användarinmatat livsmedel'
        }

    def set_new_name(self, victual_number, victual_name):
        self.loc[self['Nummer'] == victual_number, 'Namn'] = victual_name

    def interactively_set_new_name(self, victual_number):
        name = input('Ange nytt namn ["q" för att avsluta] {' + self[self["Nummer"] == victual_number]["Namn"].values[0] + '}: ')
        if name == 'q' or name == '' or name == self[self["Nummer"] == victual_number]["Namn"].values[0]:
            return None
        self.set_new_name(victual_number, name)
        return name

    def get_groups(self):
        l = list(set(self['Huvudgrupp'].values))
        l.sort()
        return l

    def get_names(self):
        return self['Namn'].values.tolist()

    def get_amounts(self):
        return self['ViktGram'].values.tolist()

class RecipeDataFrame(SearchableDataFrame):

    def interactively_get_new_recipe_row(self):
        number = self['Nummer'].max()
        number = 1 if np.isnan(number) else number+1
        name = input('Vänligen ange ett namn på receptet: ')
        if name == '':
            return None
        description = input('Skriv in en receptbeskrivning: ')
        if description == '':
            return None
        return {'Nummer': number, 'Namn': name, 'Beskrivning': description}

    def set_new_name(self, recipe_number, recipe_name):
        self.loc[self['Nummer'] == recipe_number, 'Namn'] = recipe_name

    def interactively_set_new_name(self, recipe_number):
        name = input('Ange nytt namn ["q" för att avsluta] {' + self[self["Nummer"] == recipe_number]["Namn"].values[0] + '}: ')
        if name == 'q' or name == '' or name == self[self["Nummer"] == recipe_number]["Namn"].values[0]:
            return None
        self.set_new_name(recipe_number, name)
        return name

    def set_new_description(self, recipe_number, recipe_description):
        self.loc[self['Nummer'] == recipe_number, 'Beskrivning'] = recipe_description

    def interactively_set_new_description(self, recipe_number):
        print(f'Tidigare beskrivning: {self[self["Nummer"] == recipe_number]["Beskrivning"].values[0]}')
        description = input('Ange ny beskrivning ["q" för att avsluta]: ')
        if description == 'q' or description == '' or description == self[self["Nummer"] == recipe_number]["Beskrivning"].values[0]:
            return None
        self.set_new_description(recipe_number, description)
        return description

class UserDataFrame(SearchableDataFrame):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if 'Datum' in self.keys():
            try:
                self.loc[:,'Datum'] = pd.to_datetime(self['Datum'])
            except TypeError:
                pass

        # Compatibility for old versions
        if 'Aktivitet' in self.keys():
            self.loc[:,'Aktivitet'] = self['Aktivitet'].map(self._make_list_string)

    def drop_date(self, date):
        if date not in self['Datum'].values and self.verbose:
            print(f'Got {date} of type {type(date)}, not found in the data frame, which instead has for example:')
            for d in self['Datum'].values[:5]:
                print(f'    Date: {d} ({type(d)})')
        return UserDataFrame(
            self.drop(self[self['Datum'] == date].index.values[0]))

    def _make_list_string(self, s):
        try:
            eval(s)
            return s
        except SyntaxError:
            return s.replace('. ','.0, ').replace('.]', '.0]')

class NutritionDataFrame(SearchableDataFrame):

    def __init__(self, *args, **kwargs):
        if 'clean_frame' in kwargs.keys():
            clean = kwargs['clean_frame']
            del kwargs['clean_frame']
        else:
            clean = True

        super().__init__(*args, **kwargs)

        if clean:
            for column_name in self.keys():
                # make sure nan entries are strings so we can use e.g. __eq__()
                self.loc[:,column_name] = self[column_name].map(lambda val: self._make_nan_safe(val))
                if column_name == 'SenastAndrad':
                    try:
                        self.loc[:,column_name] = pd.to_datetime(self[column_name])
                    except TypeError:
                        pass

    def __add__(self, other):
        if not isinstance(other, NutritionDataFrame) and other is not None:
            raise TypeError('Only addition between two NutritionDataFrame (and None) are supported.')
        if other is None:
            # Addition with None just acts as identity operation
            return NutritionDataFrame(self[['Namn','Forkortning','Varde','Enhet','SenastAndrad']].copy())
        ndf = self[['Namn','Forkortning','Varde','Enhet','SenastAndrad']].copy()
        ndf['Varde'] += other['Varde']
        ndf.loc[:,'SenastAndrad'] = datetime.datetime.utcnow()
        ndf.loc[ndf['Namn'] == 'Avfall (skal etc.)', 'Varde'] = 0.0
        return NutritionDataFrame(ndf)

    def __mul__(self, other):
        try:
            fac = float(other)
        except TypeError:
            raise TypeError('NutritionDataFrame can only be multiplied by numbers')
        ndf = self[['Namn','Forkortning','Varde','Enhet','SenastAndrad']].copy()
        ndf['Varde'] = ndf['Varde'] * fac
        ndf.loc[ndf['Namn'] == 'Avfall (skal etc.)', 'Varde'] = 0.0
        return NutritionDataFrame(ndf)

    def drop_name(self, name):
        return NutritionDataFrame(
            self.drop(self[self['Namn'] == name].index.values[0]))

    def fix_energy_entry(self):
        """
        If either 'kcal' or 'kJ' are non-zero, the other is calculated.
        """
        if self.loc[self['Namn'] == 'Energi (kJ)', 'Varde'].values[0] == 0.0 and self.loc[self['Namn'] == 'Energi (kcal)', 'Varde'].values[0] != 0.0:
            self.loc[self['Namn'] == 'Energi (kJ)', 'Varde'] = self.loc[self['Namn'] == 'Energi (kcal)', 'Varde'].values[0] * 4.184
        if self.loc[self['Namn'] == 'Energi (kJ)', 'Varde'].values[0] != 0.0 and self.loc[self['Namn'] == 'Energi (kcal)', 'Varde'].values[0] == 0.0:
            self.loc[self['Namn'] == 'Energi (kcal)', 'Varde'] = self.loc[self['Namn'] == 'Energi (kJ)', 'Varde'].values[0] * 1/4.184
