
import pandas as pd
import numpy as np
import datetime, threading, time
from livsmedel.combodata import Victuals, Recipes, User, Users

class InteractiveSession():

    def __init__(self, victuals=None, recipes=None, users=None, verbose=False):

        self.done = False

        self.verbose = verbose

        self.thread = None

        self.verbose and print('Interactive: Loading Victuals...')
        if victuals is None:
            self.VICTUALS = None
            self.verbose and print('Interactive: Backgrounding Victuals...')
            self.thread = threading.Thread(target=self._load_victuals)
            self.thread.start()
        elif isinstance(victuals, Victuals):
            self.VICTUALS = victuals
        else:
            raise TypeError('victuals provided not a Victuals object')
        self.verbose and print('Interactive: Loading Recipes...')
        if recipes is None:
            self.RECIPES = Recipes(verbose=self.verbose)
        elif isinstance(recipes, Recipes):
            self.RECIPES = recipes
        else:
            raise TypeError('recipes provided not a Recipes object')
        self.verbose and print('Interactive: Loading Users...')
        if users is None:
            self.USERS = Users(verbose=self.verbose)
        elif isinstance(users, Users):
            self.USERS = users
        else:
            raise TypeError('users provided not a Users object')

        self.active = None
        self.session_trace = []
        self.set_active('home')

    def _load_victuals(self):
        self.VICTUALS = Victuals(verbose=self.verbose)

    def _wait_thread(self):
        """
        Every function that uses `self.VICTUALS` must call this method to make sure it
        exist.
        """
        if self.thread is None:
            return None
        if self.thread.is_alive():
            print('Väntar på att livsmedel ska laddas...')
        while self.thread.is_alive():
            time.sleep(0.1)

    def clear_screen(self):
        print(chr(27) + "[2J")
        self.verbose and print(f'Active is: "{self.active}"')

    def unknown_reply(self):
        print('Inmatad data ej förstådd. Försök igen.')

    def set_active(self, name):
        self.active = name
        self.session_trace.append(name)

    def go_back(self):
        self.verbose and print(f'Going from "{self.active}" to "{self.active.split("_")[0]}"')
        new_active = self.active.split('_')[0]
        if new_active == 'save' or new_active == 'quit':
            new_active = 'home'
        self.set_active(new_active)

    def print_trace(self):
        print('Session trace:')
        for trace in self.session_trace:
            print(trace)
        print('End of session trace.')

    def _is_integer(self, reply):
        try:
            if int(reply) - float(reply) == 0:
                return True
            return False
        except ValueError:
            return False
        return False

    def current(self):

        selected = self.active.split()[0]

        try:
            eval('self.' + selected + '()')
        except AttributeError:
            raise NotImplementedError(f'No implemented method with name: {selected}')
        except EOFError:
            raise EOFError('Interaction ended, trace:\n' + ' \n'.join(self.session_trace) + '\nEnd of trace.')
        except KeyboardInterrupt:
            self.quit()

    def home(self):
        self.clear_screen()
        homescreen = """
Skriv in:

    h - för hjälp
    l - för livsmedelsmeny
    r - för receptmeny
    a - för användarmeny
    s - för att spara
    q - för att spara och avsluta

In {q}: """
        while True:
            reply = input(homescreen).lower()
            if reply == 'h':
                self.set_active('help')
                break
            if reply == 'l':
                self.set_active('victual')
                break
            if reply == 'r':
                self.set_active('recipe')
                break
            if reply == 'a':
                self.set_active('user')
                break
            if reply == 's':
                self.set_active('save')
                break
            if reply == 'q' or reply == '':
                self.set_active('quit')
                break
            self.unknown_reply()

    def help(self):
        self.clear_screen()
        helpscreen = """
Hjälpmeddelande.

    * Förslag till användning
        - Lägg in användare under användarmenyn
        - Lägg till recept som ni ätit den senaste tiden under recept menyn
        - För varje dag, lägg till vad ni ätit för varje användare
        - Om livsmedel saknas, lägg till dem under livsmedels menyn

    * Gällande input-prompten
        - Allmän information t.ex. alternativ finns inom hakparenterser
        - Standardsvaret (om ingen input ges), om det finns ett, finns inom klammerparentes

    * Övrigt
        - Man kan avsluta vad som helst genom att trycka "Ctrl" + "C", efter det
          får man en prompt om att avbryta.

In [*Enter* för att återvända]: """
        input(helpscreen).lower()
        self.set_active('home')

#
#                "             m                  ""#
#       m   m  mmm     mmm   mm#mm  m   m   mmm     #     mmm
#       "m m"    #    #"  "    #    #   #  "   #    #    #   "
#        #m#     #    #        #    #   #  m"""#    #     """m
#         #    mm#mm  "#mm"    "mm  "mm"#  "mm"#    "mm  "mmm"
#

    def victual(self):
        self.clear_screen()
        victualscreen = """
Skriv in:

    l - lägg till livsmedel
    m - lägg till livsmedelsmix
    j - ändra livsmedel
    t - ta bort livsmedel
    i - importera livsmedel från recept
    s - sök livsmedel
    b - bläddra efter grupp

    r - spara ner livsmedel till disk

    h - hjälp
    q - återvänd

In {q}: """
        help = """
Detaljerad beskrivning av alternativen.

    l - Kommer be dig att lägga till en livsmedelsprodukt, d.v.s. namn och näringsvärden.
        Alla näringsvärden behövs inte läggas till, det räcker med dem du har på produkten.
        Du kan avbryta införandet när som helst genom att trycka `Ctrl+C`.
    m - Om du har en produkt där innehållsprodukterna är tydligt men inte andelen, då kan
        du använda detta alternativ för att ange de näringsvärden som du har på produkten,
        och programmet kommer uppskatta (via linjäranpassning) andelen av innehållsprodukterna.
        T.ex. en påse blandade nötter. Detta kan vara bra om du försöker ha bättre koll på
        de näringsvärden inte listade på förpackningen. Du kan avbryta införandet när som
        helst genom att trycka `Ctrl+C`.
    t - Om ett livsmedel ska tas bort så kan under detta alternativ välja livsmedel för radering.
        Endast användarinförda livsmedel får tas bort, så att Livsmedelsverkets inlägg är intakta.
        När ett livsmedel har valts för radering så kollas att recept inte beror på det livsmedel,
        och om det finns beroende recept ombeds du ta bort dem med.
    i - Detta alternativet låter dig flytta ett hämta ett recept och föra in här, i
        livsmedelstabellen. Detta kan användas när man t.ex. har ett recept för ett hembakat
        bröd och vill göra ett nytt frukostrecept med det brödet.
    s - Kommer be om sökord och du kan söka livsmedel efter namn och ger 10 resultat åt gången.
        Sökningen sker inte exakt, och för att hitta rätt resultat behövs kanske några olika
        försök. T.ex. 'lök' har 'Vitlök' som bäst resultat, medans 'Lök gul' är först på 9:e
        plats, eller 'vatten' så finns 'Vatten kranvatten' på 7:e plats, efter t.ex. 'Valnötter'.
    b - Här listas alla kategorier som livsmedlet är indelat efter, samt en 'Användarinmatat
        livsmedel' som innehåller allt som du inmatat med alternativen 'l' eller 'm'. Bläddringen
        kan vara bra om man inte hittar rätt vara med en sökning ('s'). T.ex. hittar man inte
        'Morot' för att man söker 'morötter' (där alternativet inte finns) så man här se gruppen
        'Rotfrukter' och se att namnet för livsmedlet 'Morot'.

In [*Enter* för att återvända]: """
        while True:
            reply = input(victualscreen).lower()
            if reply == 'l':
                self.set_active('victual_add')
                break
            if reply == 'm':
                self.set_active('victual_mix')
                break
            if reply == 'j':
                self.set_active('victual_modify')
                break
            if reply == 't':
                self.set_active('victual_delete')
                break
            if reply == 'i':
                self.set_active('victual_import')
                break
            if reply == 's':
                self.set_active('victual_search')
                break
            if reply == 'b':
                self.set_active('victual_browse')
                break
            if reply == 'r':
                self.set_active('victual_save')
                break
            if reply == 'h':
                input(help)
                continue
            if reply == 'q' or reply == '':
                self.set_active('home')
                break
            self.unknown_reply()

    def victual_add(self):
        self.clear_screen()
        self._wait_thread()
        self.VICTUALS.interactively_add_victual()
        self.go_back()

    def victual_mix(self):
        self.clear_screen()

        name = input('Ange namn på produkten: ')

        # Get the nutritional values and names
        self._wait_thread()
        ndf = self.VICTUALS._interactively_get_new_nutrition_frame(correct=False)
        # If energy is present, it will be present in two units; kcal and kJ. We can use
        # either or both.
        # As numeric values, kJ is larger than kcal since a kJ measures less energy
        # so using kJ means it dominates the optimization calculation we will perform
        # shortly, with the loss of accuracy for other nutritional values.
        # We choose to prioritize energy to some degree by choosing kJ, but dropping kcal.
        ndf.fix_energy_entry()
        ndf = ndf.drop_name('Energi (kcal)')
        ndf = ndf[ndf['Varde'] != 0]

        self.verbose and print(ndf)

        n_names = ndf['Namn'].values
        n_values = np.matrix([ndf['Varde'].values]).T

        # Get the ingredients on the package
        idf = self.VICTUALS.interactively_get_new_victual_frame(ask_amounts=False)
        i_numbers = idf['Nummer'].values
        i_names = idf['Namn'].values

        nutrition_matrix = []
        for nr in i_numbers:
            frame = self.VICTUALS.dictionary[nr]
            values = frame[frame['Namn'].map(lambda n: n in n_names)]['Varde'].values
            nutrition_matrix.append(values)
        nutrition_matrix = np.matrix(nutrition_matrix).T

        # Linear optimization
        ratios = np.array(
            (nutrition_matrix.T * nutrition_matrix).I * nutrition_matrix.T * n_values
        ).flatten()

        print('Lösningen är:')
        for i in range(len(i_names)):
            print(f'    {round(ratios[i]*100, 1)}%: {i_names[i]}')
        if any(ratios < 0) or any(ratios > 1.5) or sum(abs(ratios)) > 1.5:
            # if some ingredients have beend missed by mistake, or purposfully left out by
            # the user, what may happen is that the ratios come out wrong. It may happen
            # naturally that one ingredient have a ratio being slightly larger than 1.0 (e.g.
            # 1.01 is likely within errors). But if a ratio is as high as 1.5 or higher, something
            # is probably wrong. I a ratio is negative or the sum of all are too large it is
            # also a sign of something gone wrong.
            #
            # Proof of concept in numbers:
            #
            #                   Energy  Fat  Carb.
            #  Input by user:     2000    5     5
            #
            #  From victuals:
            #   ingredient_1:     2000    4     8
            #   ingredient_2:     1000    1     7
            #
            #  Solution: 1.5*ingredient_1 - 1.0*ingredient_2
            while True:
                msg =  'Någon andel verkar vara fel.\n'
                msg += '\n'
                msg += '    g - godkänn andelarna och fortsätt i alla fall\n'
                msg += '    q - avbryt införandet helt\n'
                msg += '\n'
                msg += 'In {q}: '
                reply = input(msg).lower()
                if reply == 'q' or reply == '':
                    self.set_active('victual')
                    return None
                if reply == 'g':
                    break
        else:
            input('[*Enter* för att fortsätta]: ')

        # Generate the Victual row
        new_victual_row = self.VICTUALS._interactively_get_new_victual_row(name)

        # Generate the nutrition dataframe
        new_nutrition_dataframe = None
        for i in range(len(i_numbers)):
            tmp_frame = self.VICTUALS.dictionary[i_numbers[i]] * ratios[i]
            new_nutrition_dataframe = tmp_frame + new_nutrition_dataframe

        #
        nndf = new_nutrition_dataframe[new_nutrition_dataframe['Namn'].map(lambda n: n in n_names)]
        print(pd.DataFrame({
            'Namn': ndf['Namn'].values,
            'Inmatat värde (x)': list(map(lambda x: round(x,3), ndf['Varde'].values)),
            'Anpassat värde (y)': list(map(lambda x: round(x,3), nndf['Varde'].values)),
            'Fel i anpassning ((y-x)/x, %)': list(map(lambda x: round(x,1), (nndf['Varde'].values - ndf['Varde'].values) / ndf['Varde'].values * 100))
        }))
        print()
        input('[*Enter* för att fortsätta (lägger till livsmedlet)]: ')

        self.VICTUALS.add_victual(new_victual_row, new_nutrition_dataframe)

        self.go_back()

    def victual_modify(self):
        self.clear_screen()
        self._wait_thread()
        victual_nr = self.VICTUALS.interactively_select(extra_prompt='Nummer för att välja för ändring', restrict_col='Huvudgrupp', restrict_val='Användarinmatat livsmedel')
        if victual_nr is None:
            self.go_back()
            return None
        change = False
        while True:
            s =  '\n'
            s += '  n - Namn\n'
            s += '  m - Näringsvärden\n'
            print(s)
            reply = input('Välj fält att ändra ["q" för att avsluta] {q}: ').lower()
            if reply == '' or reply == 'q':
                break
            if reply == 'n':
                name = self.VICTUALS.interactively_set_new_name(victual_nr)
                if name is not None:
                    change = True
                continue
            if reply == 'm':
                df = self.VICTUALS.interactively_change_nutritional_frame(victual_nr)
                if df is not None:
                    change = True
                continue
        if change:
            #TODO: when changed, scan recipes and see if this victual has been used,
            # prompt user to accept generate new nutritional frame for those recipes
            pass

    def victual_delete(self):
        self.clear_screen()
        self._wait_thread()
        victual_nr = self.VICTUALS.interactively_select(extra_prompt='Nummer för att välja för radering', restrict_col='Huvudgrupp', restrict_val='Användarinmatat livsmedel')
        if victual_nr is None:
            self.go_back()
            return None
        # find if number is in a recipe
        cont = True
        rec_df = self.RECIPES.get_recipes_with_victual(victual_nr)
        if len(rec_df) > 0:
            print('\nFöljande recept använder sig av detta livsmedel: ')
            print(rec_df)
            print()
            while True:
                print(f'Väljer du att slutföra raderingen av "{self.VICTUALS.dataframe[self.VICTUALS.dataframe["Nummer"] == victual_nr]["Namn"].values[0]}", kommer även de ovanstående recept tas bort.')
                reply = input('In ["F" för att fortsätta, "q" avslutar] {q}: ').lower()
                if reply == 'q' or reply == '':
                    self.go_back()
                    return None
                if reply == 'f':
                    break
                self.unknown_reply()
            cont = self.RECIPES.interactively_remove_recipes(rec_df['Nummer'].values)
        if not cont:
            self.go_back()
            return None
        self.VICTUALS.interactively_remove_victual(victual_nr)
        self.go_back()

    def victual_import(self, ret='victual'):
        self.clear_screen()
        # get the recipe number
        recipe_nr = self.RECIPES.interactively_select(extra_prompt='Nummer för att importera')
        if recipe_nr is None:
            self.set_active(ret)
            return None

        # Get the recipe frame with the total amount of weights
        rdf = self.RECIPES.rec_dictionary[recipe_nr]
        # Find the factor to rescale to 100g
        fac = 100/sum(rdf['ViktGram'].values)

        # Get the nutritional frame rescaled to 100g
        ndf = self.RECIPES.dictionary[recipe_nr] * fac

        # Get the name and number of the recipe
        df_row = self.RECIPES.dataframe[self.RECIPES.dataframe['Nummer'] == recipe_nr]
        name = df_row['Namn'].values[0]
        number = df_row['Nummer'].values[0]
        name = name + ' [' + str(number) + ']' # adding the number to keep track of which recipe gave rise to it

        self._wait_thread()
        new_victual_row = self.VICTUALS._interactively_get_new_victual_row(name)

        print(new_victual_row)
        input('Kommer lägga till ovanstående rad, *Enter* för att godkänna/fortsätta: ')

        print(ndf)
        input('Kommer lägga till ovanstående näringsvärdestabell, *Enter* för att godkänna/fortsätta: ')

        self.VICTUALS.add_victual(new_victual_row, ndf)

        self.set_active(ret)

    def victual_search(self):
        self.clear_screen()
        self._wait_thread()
        self.VICTUALS.interactively_print_nutrition()
        self.go_back()

    def victual_browse(self):
        self.clear_screen()
        self._wait_thread()
        self.VICTUALS.interactively_browse()
        self.go_back()

    def victual_save(self):
        self.clear_screen()
        self._wait_thread()
        self.VICTUALS.save()
        input('Livsmedel sparade, *Enter* för att återgå: ')
        self.go_back()

#
#                              "
#        m mm   mmm    mmm   mmm    mmmm    mmm    mmm
#        #"  " #"  #  #"  "    #    #" "#  #"  #  #
#        #     #""""  #        #    #   #  #""""   """m
#        #     "#mm"  "#mm"  mm#mm  ##m#"  "#mm"  "mmm"
#                                   #
#                                   "
#

    def recipe(self):
        self.clear_screen()
        recipescreen = """
Skriv in:

    a - lista alla recept
    s - sök recept
    l - lägg till recept
    t - ta bort recept
    m - ändra i recept
    e - exportera recept som livsmedel

    r - spara ner recept till disk

    h - hjälp
    q - återvänd

In {q}: """
        help ="""
Detaljerad beskrivning av alternativen.

    a - Skriver ut alla recept som du har lagt till. Här får du också tillfälle
        att välja ett recept för att se en detaljerad beskrivning av receptet, med
        detaljerad innehållslista.
    s - Här kan du söka recept om listan blivit för lång. Det finns två alternativ,
        söka efter namn eller beskrivning.
    l - Här lägger du till recept; namn, beskrivning, innehållslista med vikt på
        produkterna som används för receptet.
    t - För att söka och välja recept att ta bort ur databasen.
    m - Ändra ett recept på något sätt.
    e - Detta alternativet låter dig flytta ett recept härifrån till livsmedelstabellen.
        Detta kan användas när man t.ex. har ett recept för ett hembakat bröd och
        vill göra ett nytt frukostrecept med det brödet.

In [*Enter* för att återvända]: """
        while True:
            reply = input(recipescreen).lower()
            if reply == 'a':
                self.set_active('recipe_all')
                break
            if reply == 'l':
                self.set_active('recipe_add')
                break
            if reply == 't':
                self.set_active('recipe_delete')
                break
            if reply == 'm':
                self.set_active('recipe_modify')
                break
            if reply == 's':
                self.set_active('recipe_search_menu')
                break
            if reply == 'e':
                self.set_active('recipe_export')
                break
            if reply == 'r':
                self.set_active('recipe_save')
                break
            if reply == 'h':
                input(help)
                continue
            if reply == 'q' or reply == '':
                self.set_active('home')
                break
            self.unknown_reply()

    def recipe_all(self):
        self.clear_screen()
        while True:
            print(self.RECIPES)
            print()
            reply = input('In [Ange nummer för detaljerat recept, eller "q" för att avsluta] {q}: ').lower()
            if reply == 'q' or reply == '':
                break
            if self._is_integer(reply):
                reply = int(reply)
                if reply in self.RECIPES.dataframe['Nummer'].values:
                    self.clear_screen()
                    self.RECIPES.print_long(reply)
                    while True:
                        nd = input('In ["n" för näringsvärdestabell, "q" för att återvända] {q}: ').lower()
                        if nd == 'q' or nd == '':
                            break
                        if nd == 'n':
                            self.clear_screen()
                            print(f'Namn: {self.RECIPES.dataframe[self.RECIPES.dataframe["Nummer"] == reply]["Namn"].values[0]}')
                            print()
                            print(self.RECIPES.dictionary[reply])
                            print()
                            input('In [*Enter* för att återvända]: ')
                            break

        self.go_back()

    def recipe_add(self):
        self.clear_screen()
        self._wait_thread()
        self.RECIPES.interactively_add_recipe(self.VICTUALS)
        self.go_back()

    def recipe_delete(self):
        self.clear_screen()
        recipe_nr = self.RECIPES.interactively_select(extra_prompt='Nummer för att välja för radering')
        if recipe_nr is None:
            self.go_back()
            return None
        self.RECIPES.interactively_remove_recipe(recipe_nr)
        self.go_back()

    def recipe_modify(self):
        self.clear_screen()
        recipe_nr = self.RECIPES.interactively_select(extra_prompt='Nummer för att välja för ändring')
        if recipe_nr is None:
            self.go_back()
            return None
        while True:
            self.RECIPES.print_long(recipe_nr)
            s =  '\n'
            s += '  n - Namn\n'
            s += '  b - Beskrivning\n'
            s += '  i - Innehåll\n'
            s += '\n'
            print(s)
            reply = input('Välj fält att ändra ["q" för att avsluta] {q}: ').lower()
            if reply == '' or reply == 'q':
                self.go_back()
                return None
            if reply == 'n':
                name = self.RECIPES.interactively_set_new_name(recipe_nr)
                if name is not None:
                    self.session_trace.append(f'recipe_modify n {name}')
                else:
                    self.session_trace.append(f'recipe_modify n NOT CHANGED')
                continue
            if reply == 'b':
                description = self.RECIPES.interactively_set_new_description(recipe_nr)
                if description is not None:
                    self.session_trace.append(f'recipe_modify b {description}')
                else:
                    self.session_trace.append(f'recipe_modify b NOT CHANGED')
                continue
            if reply == 'i':
                self.session_trace.append(f'recipe_modify i start: {"; ".join(self.RECIPES.rec_dictionary[recipe_nr]["Namn"].values)};; {"; ".join(map(str, self.RECIPES.rec_dictionary[recipe_nr]["ViktGram"].values))}')
                self._wait_thread()
                new_victual_dataframe = self.VICTUALS.interactively_get_new_victual_frame(start_df=self.RECIPES.rec_dictionary[recipe_nr])
                self.RECIPES.rec_dictionary[recipe_nr] = new_victual_dataframe
                new_nutritional_dataframe = None
                for number in new_victual_dataframe['Nummer'].values:
                    fac = float(new_victual_dataframe[new_victual_dataframe['Nummer'] == number]['ViktGram'].values/100)
                    tmp_frame = self.VICTUALS.dictionary[number] * fac
                    new_nutritional_dataframe = tmp_frame + new_nutritional_dataframe
                self.RECIPES.dictionary[recipe_nr] = new_nutritional_dataframe
                self.session_trace.append(f'recipe_modify i finish: {"; ".join(self.RECIPES.rec_dictionary[recipe_nr]["Namn"].values)};; {"; ".join(map(str, self.RECIPES.rec_dictionary[recipe_nr]["ViktGram"].values))}')
                self.RECIPES.saved = False
                continue
        self.go_back()

    def recipe_search_menu(self):
        self.clear_screen()
        searchscreen = """
Skriv in:

    n - sök recept efter namn
    b - sök recept efter beskrivning
    q - återvänd

In {q}: """
        while True:
            reply = input(searchscreen).lower()
            if reply == 'n':
                self.set_active('recipe_search Namn')
                break
            if reply == 'b':
                self.set_active('recipe_search Beskrivning')
                break
            if reply == 'q' or reply == '':
                self.go_back()
                break

    def recipe_search(self):
        self.clear_screen()
        _, entry = self.active.split()
        self.RECIPES.interactively_print_nutrition(column=entry)
        self.set_active('recipe_search_menu')

    def recipe_export(self):
        self.victual_import('recipe')

    def recipe_save(self):
        self.clear_screen()
        self.RECIPES.save()
        input('Recept sparade, *Enter* för att återgå: ')
        self.go_back()

#
#        m   m   mmm    mmm    m mm   mmm
#        #   #  #   "  #"  #   #"  " #   "
#        #   #   """m  #""""   #      """m
#        "mm"#  "mmm"  "#mm"   #     "mmm"
#

    def user(self):
        self.clear_screen()
        userscreen = """
Skriv in:

    n - lägg till ny dag
    j - ändra införd dag
    d - ta bort dag

    l - lägg till användare
    t - ta bort användare
    m - modifiera användare

    p - skriv ut användarprofil
    s - skriv ut användartabell

    r - spara ner användare till disk

    h - hjälp
    q - återvänd

In {q}: """
        help = """
Detaljerad beskrivning av alternativen.

    n - När du vill fylla i vad som en användare har ätit under en dag, kan det
        göras här.
    l - Här kan du lägga till nya användare.
    t - Här kan du ta bort en användare. Tar du bort en användare så kommer all
        data infört för den användaren försvinna, d.v.s. det som de ätit.
    m - Här kan man ändra en användares data för hand, om något blivit fel. T.ex.
        namn, längd, etc.
    p - Användarprofilen består av lite beräknad data för användaren, och du kan
        välja användare och skriva ut deras profil här. Profilen kommer uppdateras
        efter nya dagar och vad som ätits läggs till.
    s - Användartabellen är en tabell över vad som ätits på vilka dagar. Du kan välja
        användare och skriva ut deras användartabeller här.

In [*Enter* för att återvända]: """
        while True:
            reply = input(userscreen).lower()
            if reply == 'n':
                self.set_active('user_pick add_day')
                break
            if reply == 'j':
                self.set_active('user_pick modify_day')
                break
            if reply == 'd':
                self.set_active('user_pick remove_day')
                break
            if reply == 'l':
                self.set_active('user_add')
                break
            if reply == 't':
                self.set_active('user_pick remove')
                break
            if reply == 'm':
                self.set_active('user_pick modify')
                break
            if reply == 'p':
                self.set_active('user_pick print_profile')
                break
            if reply == 's':
                self.set_active('user_pick print_table')
                break
            if reply == 'r':
                self.set_active('user_save')
                break
            if reply == 'h':
                input(help)
                continue
            if reply == 'q' or reply == '':
                self.set_active('home')
                break
            self.unknown_reply()

    def user_add(self):
        self.clear_screen()
        new_user = User()
        self.USERS.append(new_user)
        self.go_back()

    def user_pick(self):
        """
        Displays a list of users to pick from. Next active is selected by passing
        a name with the `self.active`, that is `user_pick {name}`, and self.active
        will be set to `user_{name} {number}`, where `number` is selected user
        (according to position in self.USERS).
        """
        self.clear_screen()
        _, name = self.active.split()
        self.verbose and print(f'Entering `user_pick` with `name={name}`')
        sel = self.USERS.interactively_select_user()
        if sel == 'q' or sel is None:
            self.go_back()
        else:
            self.set_active('user_' + name + f' {sel}')

    def _unique_date(self, user, date):
        """
        Returns False if the user does not wish to proceed with the input date.
        """
        if date in user.dataframe['Datum'].values:
            while True:
                reply = input(f'Det finns redan ett inlägg för datum {pd.to_datetime(date).date()}. Skriva över [j/n]: ').lower()
                if reply == 'n':
                    return False
                if reply == 'j':
                    user.dataframe = user.dataframe.drop_date(date)
                    break
                self.unknown_reply()
        return True

    def user_add_day(self):
        self.clear_screen()
        _, nr = self.active.split()
        nr = int(nr)
        while True:
            default_date = np.datetime64(datetime.date.today())
            reply = input('Ange datum [YYYY-MM-DD, "q" avslutar] {' + f'{pd.to_datetime(default_date).date()}' +'}: ').lower()
            if reply == '':
                date = default_date
            elif reply == 'q':
                self.set_active('user_pick add_day')
                return None
            else:
                try:
                    date = pd.DatetimeIndex([reply]).values[0]
                except (ValueError,TypeError):
                    self.unknown_reply()
                    continue
            cont = self._unique_date(self.USERS.user_list[nr], date)
            if not cont:
                continue
            else:
                break
        recipe_list, nutrition_table = self.RECIPES.interactively_get_recipe_list()
        if len(recipe_list) == 0:
            print('Inga recept angivna. Avslutar.')
            self.set_active('user_pick add_day')
            return None
        self.USERS.interactively_add_new_user_line(nr, date, recipe_list, nutrition_table)
        self.set_active('user_pick add_day')

    def user_modify_day(self):
        _, nr = self.active.split()
        nr = int(nr)
        date = self.USERS.interactively_select_day(nr)
        if date is not None:
            self.USERS.interactively_modify_day(nr, self.RECIPES, date)
        self.set_active('user_pick modify_day')

    def user_remove_day(self):
        _, nr = self.active.split()
        nr = int(nr)
        date = self.USERS.interactively_select_day(nr)
        if date is not None:
            self.USERS.interactively_remove_day(nr, date)
        self.set_active('user_pick remove_day')

    def user_print_profile(self):
        self.clear_screen()
        _, nr = self.active.split()
        nr = int(nr)
        user = self.USERS.user_list[nr]
        # make sure user profile will be up to date
        user.update()
        print(user)
        input('In [*Enter* för att återvända]: ')
        self.set_active('user_pick print_profile')

    def user_print_table(self):
        self.clear_screen()
        _, nr = self.active.split()
        nr = int(nr)
        print(self.USERS.user_list[nr].dataframe)
        input('In [*Enter* för att återvända]: ')
        self.set_active('user_pick print_table')

    def user_remove(self):
        self.clear_screen()
        _, nr = self.active.split()
        nr = int(nr)
        print('Kommer nu ta bort följande användare:')
        print(self.USERS.user_list[nr])
        while True:
            reply = input('Godkänn. In [T(a bort), A(vbryt)] {a}: ').lower()
            if reply == 't':
                self.USERS.remove(nr)
                print('Användare borttagen.')
                break
            if reply == 'a' or reply == '':
                print('Avbryter.')
                break
            self.unknown_reply()
        reply = input('In [*Enter* för att återvända]: ')
        self.set_active('user_pick remove')

    def user_modify(self):
        self.clear_screen()
        _, nr = self.active.split()
        nr = int(nr)
        self.USERS.interactively_modify(nr)
        self.set_active('user_pick modify')

    def user_save(self):
        self.clear_screen()
        self.USERS.save()
        input('Användare sparade, *Enter* för att återgå: ')
        self.go_back()

    def _save(self):
        if not self.RECIPES.saved:
            while True:
                reply = input('Recepttabellen har ändrats och inte sparats. Spara? [J(a), N(ej)] {j}: ').lower()
                self.session_trace.append(f'_save recipes: {reply}')
                if reply == 'j' or reply == '':
                    self.RECIPES.save()
                    break
                if reply == 'n':
                    break
                self.unknown_reply()
        if not self.USERS.saved:
            while True:
                reply = input('Användardata har ändrats och inte sparats. Spara? [J(a), N(ej)] {j}: ').lower()
                self.session_trace.append(f'_save users: {reply}')
                if reply == 'j' or reply == '':
                    self.USERS.save()
                    break
                if reply == 'n':
                    break
                self.unknown_reply()
        # Only if the thread loading victuals is dead we have to worry about saving
        if not self.thread.is_alive():
            if not self.VICTUALS.saved:
                while True:
                    reply = input('Livsmedelstabellen har ändrats och inte sparats. Spara? [J(a), N(ej)] {j}: ').lower()
                    self.session_trace.append(f'_save victuals: {reply}')
                    if reply == 'j' or reply == '':
                        print('Sparar. Var god vänta...')
                        self.VICTUALS.save()
                        break
                    if reply == 'n':
                        break
                    self.unknown_reply()

    def save(self):
        self.clear_screen()
        self._save()
        self.go_back()

    def quit(self):
        self.clear_screen()
        self._save()
        while True:
            reply = input('Du har valt att avsluta. Säker? [J(a), N(ej)] {j}: ').lower()
            if reply == 'j' or reply == '':
                self.session_trace.append(f'quit selected: {reply}')
                break
            if reply == 'n':
                self.go_back()
                return None
            self.unknown_reply()

        print("Tack och välkommen åter.")
        self.done = True
        self.session_trace.append('quit done')
        self.verbose and self.print_trace()
