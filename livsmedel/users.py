import numpy as np
import pandas as pd

class UserProfile():

    def __init__(self, name=None, dob=None, weight=None, height=None, sex=None):

        # conversion factors
        self.MJ_to_kcal = 1000/4.184
        self.kcal_to_MJ = 1/self.MJ_to_kcal
        # BMI data
        self.normalBMI = (18.5,24.9)
        # Metabolic equivalently of task
        self.MET = np.array([1.0, 1.5, 2.0, 5.0, 10.0])

        # Set profile attributes
        if any(map(lambda x: x is None, (name, dob, weight, height, sex))):
            self._interactively_setup_profile(name, dob, weight, height, sex)
        else:
            self.name = name.replace('"',"'")
            self.dob = pd.to_datetime(dob)
            self.weight = float(weight)
            self.height = float(height)
            self.sex = sex.lower()
            self.sex = 'h' if self.sex == 'm' else self.sex
            self.sex = 'l' if self.sex == 'k' else self.sex
            if self.sex != 'l' and self.sex != 'h':
                raise ValueError('`self.sex` expected to be "l" (low) or "h" (high) extra-factor, corresponding to female and male, respectively.')

        self.age = None
        self._update_age()

        self.activity = None
        self.set_activity()

        # Derived attributes
        self.REE = None # Resting Energy Expenditure / viloenergiförbrukning
        self.PAL = None # Physical Activity Level / fysisk aktivitetsnivå
        self.EER = None # Estimated Energy Requirement / uppskattat energibehov
        self.BMI = None
        self.iBMI = None
        self.target_weight_BMI = None
        self.target_weight_iBMI = None
        self._derived_attributes()

        # Other attributes
        self.CEI = None # Current Energy Intake / aktuellt energiintag

    def __repr__(self):
        # This should be able to init a UserProfile when eval()
        if "'" in self.name:
            return f'UserProfile(\"{self.name}\", \'{self.dob.year}-{self.dob.month}-{self.dob.day}\', {self.weight}, {self.height}, \'{self.sex}\')'
        else:
            return f'UserProfile(\'{self.name}\', \'{self.dob.year}-{self.dob.month}-{self.dob.day}\', {self.weight}, {self.height}, \'{self.sex}\')'

    def __str__(self):
        self._update_profile()
        string =  f'==== Användarprofil för: {self.name} ====\n'
        string += '\n'
        string += f'Ålder:  {self.age} år\n'
        string += f'Vikt:   {self.weight} kg\n'
        string += f'Längd:  {self.height} m\n'
        string += '\n'
        string += f'Viloenergiförbrukning (beräknad): {self.REE} kcal ({round(self.REE * self.kcal_to_MJ,2)} MJ) /dag\n'
        string += '\n'
        string += f'Uppskattad fysisk aktivitetsnivå: {self.PAL} (rekommenderat: 1.8)\n'
        string += f'Uppskattat energibehov:           {self.EER} kcal ({round(self.EER * self.kcal_to_MJ,2)} MJ) /dag\n'
        if self.CEI is not None:
            string += f'Aktuellt energiintag:             {round(self.CEI,2)} kcal ({round(self.CEI*self.kcal_to_MJ,2)} MJ)\n'
            string += f'Energiintagsunderskott:           {round(self.EER - self.CEI,2)} kcal ({round((self.EER - self.CEI) * self.kcal_to_MJ,2)}) ({round((self.EER - self.CEI)/self.EER*100,2)} %)\n'
            string += f'Uppskattad viktminskning:         {round((self.EER - self.CEI) * self.kcal_to_MJ * 1/4.2,2)} kg/vecka\n'
            if round(self.EER - self.CEI,2) * self.kcal_to_MJ > 4.2:
                string += f'  Varning: Energiunderskottet överstiger 4.2 MJ/dag ~ 1kg/vecka viktminskning.\n'
        string += '\n'
        string += f'BMI:  {self.BMI} (normalviktsomfång: {self.target_weight_BMI} kg)\n'
        string += f'iBMI: {self.iBMI} (normalviktsomfång: {self.target_weight_iBMI} kg)\n'
        string += '\n'
        string += '='*len(f'==== Användarprofil för: {self.name} ====')

        return string

    def _update_profile(self):
        self._update_age()
        self._derived_attributes()

    def _update_age(self):
        self.age = int((self.dob.today() - self.dob) / pd.Timedelta('365 days'))

    def _derived_attributes(self):
        """
        Calculates and stores the derived attributes.
        """

        # resting energy expenditure
        self.REE = self.resting_energy_expenditure()
        # base metabolic rate
        #self.BMR = self.base_metabolic_rate()

        # estimate physical activity level (PAL) from activity
        self.PAL = self.physical_activity_level()
        # calculate estimated energy requirement (EER) from PAL
        self.EER = self.estimated_energy_requirement()

        # classical BMI
        self.BMI = self.body_mass_index()
        # improved BMI
        self.iBMI = self.body_mass_index_improved()

        # suggested target weight on BMI/iBMI
        self.target_weight_BMI = self.suggest_weight_BMI()
        self.target_weight_iBMI = self.suggest_weight_iBMI()

    def _normalize_activity(self, act):
        return (lambda a: list(map(lambda x: round(x, 2), a * 24/a.sum())))(np.array(act))

    def _interactively_setup_profile(self, name=None, dob=None, weight=None, height=None, sex=None):
        if name is None:
            self.name = input('Vänligen ange namn på profilen: ').replace('"',"'")
        else:
            self.name = name
        if dob is None:
            while True:
                dob = input('Vänligen ange födelsedatum på profilen [YYYY-MM-DD]: ')
                try:
                    dob = pd.to_datetime(dob)
                    break
                except:
                    self.unknown_reply()
            self.dob = dob
        else:
            self.dob = pd.to_datetime(dob)
        if weight is None:
            self.weight = self._numeric_input('Vikt (kg): ')
        else:
            self.weight = weight
        if height is None:
            self.height = self._numeric_input('Längd (m): ')
        else:
            self.height = height
        if sex is None:
            self.sex = self._sex_input()
        else:
            self.sex = sex

    def _numeric_input(self, prompt, default=None, numtype=float):
        correct_input = False
        while not correct_input:
            reply = input(prompt)
            if reply == '' and default is not None:
                return default
            try:
                reply = numtype(reply)
                correct_input = True
            except ValueError:
                self.unknown_reply()
        return reply

    def _sex_input(self):
        correct_input = False
        while not correct_input:
            reply = input('Extrafaktor/kön: ["h"/"m" hög/man, "l"/"k" låg/kvinna, "i" för info]: ').lower()
            reply = 'h' if reply == 'm' else reply
            reply = 'l' if reply == 'k' else reply
            if reply == 'i':
                print('Info: Detta är för att uppskatta basalmetabolism/viloenergiförbrukning. Profilen\nkommer inte skriva ut information om kön, och valet kan göras här efter de\nkönsneutrala "h" eller "l" för det extra bidrag en själv vet eller\nuppskattningsvis ligger närmast.')
            elif reply == 'h' or reply == 'l':
                correct_input = True
            else:
                self.unknown_reply()
        return reply

    def update_other_attributes(self, cei=None, activity=None, weight=None):
        changed = False
        if cei is not None:
            self.CEI = cei
        if activity is not None:
            changed = True
            self.activity = activity
        if weight is not None:
            changed = True
            self.weight = weight
        if changed:
            self._derived_attributes()

    def activity_input(self):
        correct_input = False
        while not correct_input:
            hours_left = 24
            rest = self._numeric_input(f'Ange antalet timmar i vila (t.ex. sovandes) [{hours_left}] ' + '{0.0}' + ': ', default=0.0)
            hours_left -= rest
            vlight = self._numeric_input(f'Ange antalet timmar i väldigt låg aktivitet (t.ex. stillasittande arbete) [{hours_left}] ' + '{0.0}' + ': ', default=0.0)
            hours_left -= vlight
            light = self._numeric_input(f'Ange antalet timmar i låg aktivitet (t.ex. stående arbete, hemarbete, lätt prommenad) [{hours_left}] ' + '{0.0}' + ': ', default=0.0)
            hours_left -= light
            moderate = self._numeric_input(f'Ange antalet timmar i måttlig aktivitet (t.ex. ansträngande promenad) [{hours_left}] ' + '{0.0}' + ': ', default=0.0)
            hours_left -= moderate
            high = self._numeric_input(f'Ange antalet timmar i hög aktivitet (t.ex. träning) [{hours_left}] ' + '{0.0}' + ': ', default=0.0)

            if not all(map(lambda x: x == 0, (rest, vlight, light, moderate, high))):
                act = self._normalize_activity((rest, vlight, light, moderate, high))
            else:
                act = [24.0, 0.0, 0.0, 0.0, 0.0]

            accepted = False
            print(f'Daglig aktivitetsfördelning: {act}\n')
            while not accepted:
                reply = input('Ser ovanstående aktivitetsfördelning korrekt ut? [j/n] {j}: ').lower()
                if reply == 'j' or reply == '':
                    accepted = True
                    correct_input = True
                elif reply == 'n':
                    accepted = True
                    correct_input = False
                else:
                    self.unknown_reply()

        return act

    def set_activity(self, act=None):
        if act is None:
            self.activity = np.array([24.0, 0.0, 0.0, 0.0, 0.0])
        else:
            self.activity = np.array(self._normalize_activity(act))

    def unknown_reply(self):
        print('Inmatad data ej förstådd. Försök igen.')

    def base_metabolic_rate(self):
        """
        Returns base metabolic rate, as estimated by the Mifflin-St Jeor Formula.
        """
        return round(10*self.weight + 6.25*self.height*100 - 5*self.age + (5 if self.sex == 'h' else -161), 0)

    def resting_energy_expenditure(self):
        """
        Returns resting energy expenditure, as estimated by Table 8.4 of "Nordic
        Nutrition Recommendations 2012: Integrating nutrition and physical activity" [1]

        [1] http://norden.diva-portal.org/smash/record.jsf?pid=diva2%3A704251&dswid=-6997
        """

        REE = 0
        if self.sex == 'l':
            if self.age < 3:
                REE = (0.127*self.weight + 2.94*self.height - 1.20)
            elif 3 <= self.age <= 10:
                REE = (0.0666*self.weight + 0.878*self.height + 1.46)
            elif 11 <= self.age <= 18:
                REE = (0.0393*self.weight + 1.04*self.height + 1.93)
            elif 19 <= self.age <= 30:
                REE = (0.0433*self.weight + 2.57*self.height - 1.180)
            elif 31 <= self.age <= 60:
                REE = (0.0342*self.weight + 2.10*self.height - 0.0486)
            else:
                REE = (0.0356*self.weight + 1.76*self.height + 0.0448)
        else:
            if self.age < 3:
                REE = (0.118*self.weight + 3.59*self.height - 1.55)
            elif 3 <= self.age <= 10:
                REE = (0.0632*self.weight + 1.31*self.height + 1.28)
            elif 11 <= self.age <= 18:
                REE = (0.0651*self.weight + 1.11*self.height + 1.25)
            elif 19 <= self.age <= 30:
                REE = (0.0600*self.weight + 1.31*self.height + 0.473)
            elif 31 <= self.age <= 60:
                REE = (0.0476*self.weight + 2.26*self.height - 0.574)
            else:
                REE = (0.0478*self.weight + 2.26*self.height - 1.070)
        return round(REE * self.MJ_to_kcal, 0)

    def physical_activity_level(self):
        """
        Calculates physical activity level according to user provided activity
        distribution. That is:

            PAL = (time)' * (MET) / 24

        (time) and (MET) being vectors.

        See Section "Reference values for energy requirements in adults" in "Nordic
        Nutrition Recommendations 2012: Integrating nutrition and physical activity" [1]
        for more information.

        [1] http://norden.diva-portal.org/smash/record.jsf?pid=diva2%3A704251&dswid=-6997
        """
        return round((self.MET * self.activity).sum()/24, 2)

    def estimated_energy_requirement(self):
        """
        Calculates the estimated energy requirement for the user, according to:

            EER = REE * PAL

        See Section "Reference values for energy requirements in adults" in "Nordic
        Nutrition Recommendations 2012: Integrating nutrition and physical activity" [1]
        for more information.

        [1] http://norden.diva-portal.org/smash/record.jsf?pid=diva2%3A704251&dswid=-6997
        """
        return round(self.REE * self.PAL, 0)

    def body_mass_index(self):
        """
        Returns body mass index, according to the formula `BMI = m/h²`.
        """
        return round(self.weight / self.height**2, 1)

    def suggest_weight_BMI(self):
        """
        Suggested weight range based on BMI
        """
        return (round(self.normalBMI[0]*self.height**2,1), round(self.normalBMI[1]*self.height**2,1))

    def body_mass_index_improved(self):
        """
        Improved Trefethen body mass index, `BMI_new = 1.3 m/h^2.5`.
        """
        return round(1.3*self.weight / self.height**2.5, 1)

    def suggest_weight_iBMI(self):
        """
        Suggested weight range based on iBMI
        """
        return (round(self.normalBMI[0]*self.height**2.5/1.3,1), round(self.normalBMI[1]*self.height**2.5/1.3,1))

    def interactively_modify(self):
        self._update_profile()
        modified = False
        while True:
            message_screen = f"""
Vänligen välj fält att ändra:

    1. Namn               ({self.name})
    2. Födelsedatum       ({self.dob.year}-{self.dob.month}-{self.dob.day} ({self.age} år))
    3. Vikt               ({self.weight} kg)
    4. Längd              ({self.height} m)
    5. Extrafaktor(/kön)  ({self.sex})

In ["q" för att avsluta]: """
            reply = input(message_screen)
            try:
                reply = int(reply)
            except ValueError:
                if reply == 'q':
                    break
                self.unknown_reply()
                continue

            if reply == 1:
                self.name = input(f'Nytt namn ({self.name}): ').replace('"',"'")
                modified = True
            elif reply == 2:
                while True:
                    age = input(f'Ny ålder ({self.age}): ')
                    try:
                        age = float(age)
                        self.age = age
                        self._update_profile()
                        modified = True
                        break
                    except ValueError:
                        self.unknown_reply()
                        continue
            elif reply == 3:
                while True:
                    weight = input(f'Ny vikt ({self.weight}): ')
                    try:
                        weight = float(weight)
                        self.weight = weight
                        self._update_profile()
                        modified = True
                        break
                    except ValueError:
                        self.unknown_reply()
                        continue
            elif reply == 4:
                while True:
                    height = input(f'Ny längd ({self.height}): ')
                    try:
                        height = float(height)
                        self.height = height
                        self._update_profile()
                        modified = True
                        break
                    except ValueError:
                        self.unknown_reply()
                        continue
            elif reply == 5:
                while True:
                    sex = input(f'Ny extrafaktor/kön ({self.sex}) ["h"/"m", "l"/"k"]: ')
                    sex = 'h' if sex == 'm' else sex
                    sex = 'l' if sex == 'k' else sex
                    if sex != 'h' and sex != 'l':
                        self.unknown_reply()
                        continue
                    self.sex = sex
                    self._update_profile()
                    modified = True
                    break
            else:
                self.unknown_reply()
                continue

        return modified
