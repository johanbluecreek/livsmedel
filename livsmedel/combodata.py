import os, pickle, datetime, re, hashlib, io
import requests, xmltodict
from fuzzywuzzy import fuzz
import numpy as np
import pandas as pd
from livsmedel.dataframes import VictualDataFrame, RecipeDataFrame, UserDataFrame, NutritionDataFrame
from livsmedel.users import UserProfile

class CombinedData():
    def __init__(self, dataframe, dictionary, verbose=False):

        self.verbose = verbose

        self.dataframe = dataframe
        self.dictionary = dictionary
        self._pad_nutritional_frames()
        self._verify_dictionary_type()

        # conversion factors
        self.MJ_to_kcal = 1000/4.184
        self.kcal_to_MJ = 1/self.MJ_to_kcal

    def _verify_dictionary_type(self):
        all_nutrition_df = all((
            isinstance(df, NutritionDataFrame)
        for key, df in self.dictionary.items()))
        if not all_nutrition_df:
            raise TypeError('The values of the `self.dictionary` dictionary must all be "NutritionDataFrame"s.')

    def _get_missing_names(self):
        """
        'NutritionDataFrame' objects may not have all the same rows, this help-
        function identifies the rows so that dummy rows can be added. See also
        `_pad_nutritional_frames()`.

        Returns a `missing_names` dict, with all keys being the missing names, and
        the values a tuple of abbreviation and unit, in that order.
        """
        diff_set = set()
        full_set = set()
        extra_dict = dict()
        for key, df in self.dictionary.items():
            new_full = full_set.union(set(df['Namn'].values))
            for name in new_full.difference(full_set):
                extra_dict[name] = (
                    df[df['Namn'] == name]['Forkortning'].values[0], # abbreviation
                    df[df['Namn'] == name]['Enhet'].values[0], # unit
                )
            full_set = new_full
            diff_set = diff_set.union(
                full_set.difference(set(df['Namn'].values)),
                set(df['Namn'].values).difference(full_set)
            )
        return { key: val for key,val in extra_dict.items() if key in diff_set}

    def _get_all_nutritional_names(self):
        """
        Returns a dictionary where keys are the names of all nutrition entries,
        and the values of the dictionary is a tuple of abbreviation and unit, in that order.
        """
        full_set = set()
        extra_dict = dict()
        for _, df in self.dictionary.items():
            new_full = full_set.union(set(df['Namn'].values))
            for name in new_full.difference(full_set):
                extra_dict[name] = (
                    df[df['Namn'] == name]['Forkortning'].values[0], # abbreviation
                    df[df['Namn'] == name]['Enhet'].values[0], # unit
                )
            full_set = new_full
        return extra_dict

    def _get_victual_groups(self):
        return self.dataframe.get_groups()

    def _pad_nutritional_frames(self):
        """
        'NutritionDataFrame' objects may not have all the same rows, this function
        pads all 'NutritionDataFrame's in the dictionary to have the same rows in
        the same order.
        """
        # Get all missing names, and if there are any, pad the appropriate dataframes
        missing_names = self._get_missing_names()
        if len(missing_names) == 0:
            return None

        new_dict = self.dictionary.copy()
        for entry_key, df in self.dictionary.items():
            # Find missing rows
            names = [ name for name in missing_names.keys() if name not in df['Namn'].values ]
            for name in names:
                # construct missing rows
                new_row = {
                    'Namn': name,
                    'Forkortning': missing_names[name][0],
                    'Varde': 0.0,
                    'Enhet': missing_names[name][1],
                    'SenastAndrad': datetime.datetime.utcnow(),
                }
                for col_key in df.keys().values:
                    if col_key not in new_row.keys():
                        new_row[col_key] = 'NaN'
                # add new row
                new_dict[entry_key] = new_dict[entry_key].append(new_row, ignore_index=True)
            # TODO: Would be nice to solve this by proper inheritance:
            new_dict[entry_key] = NutritionDataFrame(new_dict[entry_key].sort_values('Namn'))
        self.dictionary = new_dict

    def _numeric_input(self, prompt, default=None, numtype=float):
        correct_input = False
        while not correct_input:
            reply = input(prompt)
            if reply == '' and default is not None:
                return default
            try:
                reply = numtype(reply)
                correct_input = True
            except ValueError:
                self.unknown_reply()
        return reply

    def _is_integer(self, reply):
        return self.dataframe._is_integer(reply)

    def fuzzy_search(self, column, phrase, num=10, verbose=False, restrict_col=None, restrict_val=None):
        return self.dataframe.fuzzy_search(column, phrase, num=num, verbose=verbose, restrict_col=restrict_col, restrict_val=restrict_val)

    def _print_nutritional_table(self, idx):
        if idx in self.dictionary.keys():
            print(self.dictionary[idx])

    def interactively_select(self, column='Namn', exit_prompt='"q" för att återvända', extra_prompt=None, restrict_col=None, restrict_val=None, full_table=False):
        return self.dataframe.interactively_select(
            column=column,
            exit_prompt=exit_prompt,
            extra_prompt=extra_prompt,
            restrict_col=restrict_col,
            restrict_val=restrict_val,
            full_table=full_table,
            verbose=self.verbose
        )

    def interactively_print_nutrition(self, column='Namn'):
        reply = -1
        while reply is not None:
            reply = self.interactively_select(column, extra_prompt='Nummer för Näringsvärdestabell')
            self._print_nutritional_table(reply)

    def interactively_browse(self):
        while True:
            group = self.interactively_select_group()
            if group is None:
                return None
            current_frame = self.dataframe[self.dataframe['Huvudgrupp'] == group]
            while True:
                print(current_frame)
                reply = input('In [Nummer för Näringsvärdestabell, "q" för att återvända] {q}: ')
                if reply == 'q' or reply == '':
                    break
                if self._is_integer(reply):
                    reply = int(reply)
                    if reply in current_frame['Nummer'].values:
                        print(self.dictionary[reply])
                        reply = input('In [*Enter* för att återvända]: ')
                        continue

    def interactively_select_group(self):
        message_string  = 'Grupper:\n'
        message_string += '\n'
        groups = self._get_victual_groups()
        num = 1
        for group in groups:
            message_string += f'    {num}. {group}\n'
            num += 1
        message_string += '\n'
        message_string += f'In [1-{num-1},' + ' "q" för att återvända] {q}: '
        while True:
            reply = input(message_string).lower()
            if reply == '' or reply == 'q':
                return None
            if self._is_integer(reply):
                reply = int(reply)
                if 0 < reply < num:
                    return groups[reply-1]
            self.unknown_reply()

    def unknown_reply(self):
        print('Inmatad data ej förstådd. Försök igen.')

#
#                "             m                  ""#
#       m   m  mmm     mmm   mm#mm  m   m   mmm     #     mmm
#       "m m"    #    #"  "    #    #   #  "   #    #    #   "
#        #m#     #    #        #    #   #  m"""#    #     """m
#         #    mm#mm  "#mm"    "mm  "mm"#  "mm"#    "mm  "mmm"
#

class Victuals(CombinedData):
    def __init__(self, storage_dir=None, date='20210101', force=False, verbose=False):

        self.verbose = verbose

        self.saved = False

        # setup dir
        if storage_dir is None:
            self.storage_dir = os.environ['HOME'] + '/.livsmedel'
        else:
            self.storage_dir = storage_dir

        if not os.path.isdir(self.storage_dir):
            os.mkdir(self.storage_dir)

        # setup and load from file
        self.storage_file = self.storage_dir + '/victuals.pickle'
        self.storage_file_basename = self.storage_dir + '/victuals'
        loaded = False
        if os.path.isfile(self.storage_file) and not force:
            self.verbose and print('Victuals: Loading data from file...')
            loaded = self.load()
            if loaded:
                self.saved = True
                super().__init__(self.dataframe, self.dictionary, verbose=self.verbose)
            else:
                self.verbose and print('Victuals: Failed to load from file.')

        # refetch if not read from file
        if not hasattr(self, 'dataframe') or not hasattr(self, 'dictionary') or force:
            self.verbose and print('Victuals: Downloading data...')
            link = "http://www7.slv.se/apilivsmedel/LivsmedelService.svc/Livsmedel/Naringsvarde/" + date
            r = requests.get(link, headers={})
            self.verbose and print('Victuals: Processing data...')
            content = r._content
            content_dict = xmltodict.parse(content)
            victual_list = content_dict['LivsmedelDataset']['LivsmedelsLista']['Livsmedel']
            nutrition_dict = {}
            for victual in victual_list:
                # fix type of victual entries
                victual['Nummer'] = int(victual['Nummer'])
                victual['ViktGram'] = float(victual['ViktGram'])
                # create and add the naringsvarde dataframe
                nutrition_dict[victual['Nummer']] = NutritionDataFrame(victual['Naringsvarden']['Naringsvarde'])
                # fix type of naringsvarde entries
                nutrition_dict[victual['Nummer']]['Varde'] = nutrition_dict[victual['Nummer']]['Varde'].map(
                    lambda entry: float(entry.replace(',', '.').encode('ascii','ignore'))
                )
                nutrition_dict[victual['Nummer']]['SenastAndrad'] = pd.to_datetime(nutrition_dict[victual['Nummer']]['SenastAndrad'])
                del victual['Naringsvarden']
            victual_df = VictualDataFrame(victual_list)

            super().__init__(victual_df, nutrition_dict, verbose=self.verbose)

            self.save()

    def _verify_types(self):
        if not isinstance(self.dataframe, VictualDataFrame):
            TypeError('The dataframe `self.dataframe` must be a "VictualDataFrame".')
        self._verify_dictionary_type()

    def preview_recipe(self, ingredients, amounts):
        """
        Given list of `ingredients` ('Nummer' entry) and corresponding `amounts` (in
        grams), returns a dataframe containing the appropriate rows.

        This would be a preview for the user before adding to the global recipe objects.
        """
        if len(ingredients) != len(amounts):
            raise RuntimeError('Ingredients and amounts mismatch.')
        recept_df = pd.DataFrame(columns=self.dataframe.keys())
        for ingredient in ingredients:
            recept_df = pd.concat([recept_df,self.dataframe[self.dataframe['Namn'] == ingredient].copy()])
        recept_df['ViktGram'] = recept_df['ViktGram'] * amounts / 100
        return VictualDataFrame(recept_df)

    def interactively_get_new_victual_frame(self, start_df=None, ask_amounts=True):
        ############# TODO ###########
        # * Add possibility to add a 'victuals' entry if ingredient not found

        final_df = start_df
        ingredients = []
        amounts = []
        if final_df is not None:
            ingredients = start_df.get_names()
            amounts = start_df.get_amounts()
            print(start_df)
        else:
            print('Sök efter livsmedel att lägga till.')

        while True:
            # user selects victual
            victual_nr = self.interactively_select(extra_prompt='Nummer för att lägga till', exit_prompt='"q" för att få överblick/ändra nuvarande tabell')
            if victual_nr is not None:
                # user want to add something
                if ask_amounts:
                    amount = -1.0
                    while amount < 0:
                        # get a positive or zero amount
                        amount = self._numeric_input('Ange vikt (gram) [noll ("0" eller "0.0") avbryter]: ')
                    if amount != 0:
                        # Only add the entry if amount is nonzero
                        ingredients.append(self.dataframe[self.dataframe['Nummer'] == victual_nr]['Namn'].values[0])
                        amounts.append(amount)
                    else:
                        print('Noll mängd angivet, lägger inte till.')
                else:
                    ingredients.append(self.dataframe[self.dataframe['Nummer'] == victual_nr]['Namn'].values[0])
                    amounts.append(100.0)
            else:
                # user wants to get overview to potentially modify or exit
                final_df = self.preview_recipe(ingredients, amounts)
                while True:
                    victual_nr = final_df.interactively_select(extra_prompt='Nummer för att förändra', exit_prompt='"q" för att fortsätta/avsluta', full_table=True)
                    if victual_nr is None:
                        # user wants to continue or quit
                        while True:
                            reply = input('*F*ortsätt/*A*vbryt/*G*odkänn {f}: ').lower()
                            if reply == 'f' or reply == '':
                                break
                            if reply == 'a':
                                return None
                            if reply == 'g':
                                return final_df
                        break
                    else:
                        # user wanted to modify
                        name = self.dataframe[self.dataframe['Nummer'] == victual_nr]['Namn'].values[0]
                        idx = ingredients.index(name)
                        if ask_amounts:
                            amount = -1.0
                            while amount < 0:
                                amount = self._numeric_input('Ny vikt (gram, noll tar bort) {' + str(amounts[idx]) + '}: ', default=amounts[idx])
                            if amount == 0:
                                del ingredients[idx]
                                del amounts[idx]
                            else:
                                amounts[idx] = amount
                            break
                        else:
                            while True:
                                reply = input(f'Ta bort "{name}"?'+' [J(a),N(ej)] {j}: ')
                                if reply == 'j' or reply == '':
                                    del ingredients[idx]
                                    del amounts[idx]
                                    break
                                if reply == 'n':
                                    break
                                self.unknown_reply()
                            break

                    final_df = self.preview_recipe(ingredients, amounts)

    def _interactive_nutrition_frame(self):
        reply = None
        while True:
            msg =  'Välj alternativ:\n'
            msg += '\n'
            msg += '    l - För in näringsvärden genom att tillfrågas varje möjligt värde\n'
            msg += '    s - För in en lista av "(Namn, Värde)" par manuellt\n'
            msg += '\n'
            msg += 'In {s}: '
            reply = input(msg).lower()
            if reply == 's' or reply == '':
                return self._manually_enter_nutrition_frame()
            if reply == 'l':
                return self._interactively_enter_nutrition_frame()
            self.unknown_reply()

    def _interactively_enter_nutrition_frame(self, start_values=None, ignore_zero=False):
        # TODO: Write a short version of this where you enter a name of the entry,
        # it does a fuzzy_search and selects that entry, then asks for amount.
        # on q it quits. Similar to how a recipe is added. Then the rest are just
        # padded with zeros

        name_dict = self._get_all_nutritional_names()

        default_values = None
        if start_values is not None:
            if isinstance(start_values, NutritionDataFrame):
                default_values = {name: value for name, value in zip(start_values['Namn'].values.tolist(), start_values['Varde'].values.tolist())}
            else:
                # assumed to be a dict...
                default_values = start_values
        else:
            default_values = {name: 0.0 for name in name_dict.keys()}

        name_column = []
        abbreviation_column = []
        value_column = []
        unit_column = []

        num = 1
        if not ignore_zero:
            total = len(name_dict)
        else:
            total = len([ i for i in start_values['Varde'].values if i != 0.0])
        for name, tup in sorted(list(name_dict.items()), key=lambda x: x[0]):
            abr, unit = tup
            if ignore_zero and default_values[name] == 0.0:
                value = 0.0
            elif name == 'Niacinekvivalenter':
                value = self._numeric_input(f'Ange mängd "{name}" i varan i enheten "{unit}" per 100g.\n  Står endast "Niacin" på varan i "mg" är det ett annat fält och detta kommer istället beräknas om noll anges [{num}/{total}] ' + '{' + str(default_values[name]) + '}: ', default=default_values[name])
            elif name == 'DPA (C22:5)':
                value = self._numeric_input(f'Ange mängd "{name}" (docosapentaenoic acid) i varan i enheten "{unit}" per 100g [{num}/{total}] ' + '{' + str(default_values[name]) + '}: ', default=default_values[name])
            elif name == 'DHA (C22:6)':
                value = self._numeric_input(f'Ange mängd "{name}" (docosahexaenoic acid) i varan i enheten "{unit}" per 100g [{num}/{total}] ' + '{' + str(default_values[name]) + '}: ', default=default_values[name])
            elif name == 'EPA (C20:5)':
                value = self._numeric_input(f'Ange mängd "{name}" (eicosapentaenoic acid) i varan i enheten "{unit}" per 100g [{num}/{total}] ' + '{' + str(default_values[name]) + '}: ', default=default_values[name])
            elif name == 'Vitamin A':
                value = self._numeric_input(f'Ange mängd "{name}" i varan i enheten "{unit}" ("RE" betyder "retinolekvivalent", om angivet i "µg", antag det är "µg retinol") per 100g [{num}/{total}] ' + '{' + str(default_values[name]) + '}: ', default=default_values[name])
            else:
                value = self._numeric_input(f'Ange mängd "{name}" i varan i enheten "{unit}" per 100g [{num}/{total}] ' + '{' + str(default_values[name]) + '}: ', default=default_values[name])
            if not ignore_zero:
                num += 1
            elif default_values[name] != 0.0:
                num += 1

            name_column.append(name)
            abbreviation_column.append(abr)
            value_column.append(value)
            unit_column.append(unit)

        return name_column, abbreviation_column, value_column, unit_column

    def _manually_enter_nutrition_frame(self):
        msg =  'För in namn och mängd i samma form som exemplet nedan:\n'
        msg += '\n'
        msg += '    [(Energi (kcal), 643), (Fett, 56), (Summa mättade fettsyror, 6.3)]\n'
        msg += '\n'
        msg += 'Namn behöver inte vara exakt. Endast Svenska bokstäver, samt nummer och parenteser är tillåtna.\n'
        msg += 'In: '
        names, values, name_dict, new_names = None, None, None, None
        while True:
            reply = input(msg)
            reply = re.sub(r'\(([a-öA-Ö\s()0-9]+)', r'("\1"', reply)
            try:
                reply = eval(reply)
                if not isinstance(reply, list):
                    self.verbose and print('Did not get evaluated to a list.')
                    self.unknown_reply()
                    continue
                if len(reply) == 0:
                    self.verbose and print('Zero length list.')
                    self.unknown_reply()
                    continue
                if not all(map(lambda t: isinstance(t, tuple), reply)):
                    self.verbose and print('All entries not tuples.')
                    self.unknown_reply()
                    continue
            except (NameError, SyntaxError):
                self.verbose and print('Could not parse input.')
                self.unknown_reply()
                continue

            # find the actual nutritional names from what the user entered
            names, values = zip(*reply)
            name_dict = self._get_all_nutritional_names()
            new_names = [ (lambda k: k[(lambda l: l.index(max(l)))(list(map(lambda n: fuzz.ratio(n,name), k)))])(list(name_dict.keys())) for name in names]
            if len(new_names) != len(set(new_names)):
                print('Några inskrivna namn kunde inte skiljas åt som skilda näringsvärdestabellnamn. Försök igen.')
                continue
            break

        name_column = []
        abbreviation_column = []
        value_column = []
        unit_column = []

        # add all user enterned names
        for i in range(len(new_names)):
            name = new_names[i]
            name_column.append(name)
            value_column.append(values[i])
            abr, unit = name_dict[name]
            abbreviation_column.append(abr)
            unit_column.append(unit)
            del name_dict[name]

        # add all remaining names
        for name in name_dict.keys():
            abr, unit = name_dict[name]
            name_column.append(name)
            value_column.append(0.0)
            abbreviation_column.append(abr)
            unit_column.append(unit)

        # return columns
        return name_column, abbreviation_column, value_column, unit_column

    def _interactively_correct_nutrition_columns(self, name_column, abbreviation_column, value_column, unit_column):
        """
        This function corrects and calculates derived values when possible/appropriate,
        for example, how salt implies sodium content.

        This is not always good to do, there are for example products with sodium but
        no salt, which means that the usual ratio may be off. When trying to estimate
        the content ratio of some product, it is best not to use this function.
        """
        # Niacin equivalent (NE) calculation
        # see: https://www.livsmedelsverket.se/livsmedel-och-innehall/naringsamne/vitaminer-och-antioxidanter/niacin
        # A product can contain niacin and be implicitly present in protein, through
        # tryptofan. 60 mg tryptofan corresponds to 1 mg niacin, and if tryptofan
        # content is missing a 1% is assumed.
        ne_index = [ i for i in range(len(name_column)) if name_column[i] == 'Niacinekvivalenter' ][0]
        ne_amount = value_column[ne_index]
        if ne_amount == 0:
            tfn = 0.01
            while True:
                reply = input('Är tryptofanvärde angivet på varan? [J(a)/N(ej)] {n}: ').lower()
                if reply == 'n' or reply == '':
                    break
                if reply == 'j':
                    tfn = self._numeric_input('Ange tryptofanvärde (i gram tryptofan per gram protein): ')
                    break
            protein_index = [ i for i in range(len(name_column)) if name_column[i] == 'Protein' ][0]
            protein_amount = value_column[protein_index]
            niacin_index = [ i for i in range(len(name_column)) if name_column[i] == 'Niacin' ][0]
            niacin_amount = value_column[niacin_index]

            # unit for protein is g, while for NE is mg niacin
            value_column[ne_index] = niacin_amount + (protein_amount * tfn)/0.06

        # Sodium calculation
        # https://www.livsmedelsverket.se/livsmedel-och-innehall/naringsamne/salt-och-mineraler1/natrium
        # If sodium is entered as zero while there is salt present, we recalculate sodium based on salt.
        # We calculate this to prevent a user to falsely read a zero sodium as actual zero, which
        # could lead to an under-estimation of sodium intake.
        sodium_index = [ i for i in range(len(name_column)) if name_column[i] == 'Natrium' ][0]
        sodium_amount = value_column[sodium_index]
        salt_index = [ i for i in range(len(name_column)) if name_column[i] == 'Salt' ][0]
        salt_amount = value_column[salt_index]
        if sodium_amount == 0.0 and salt_amount != 0.0:
            # 1 g salt ≈ 0.4 g sodium; unit for sodium is mg, unit for salt is g
            value_column[sodium_index] = salt_amount * 0.4 * 1000

        # Omega-3 calculation
        # These are implicitly present in the nutrition table through DHA and EPA
        # a user may miss to enter these, or not know, so if both are zero ask
        # if omega-3 is mentioned on the product, and add to one of those entries
        dha_index = [ i for i in range(len(name_column)) if name_column[i] == 'DHA (C22:6)' ][0]
        dha_amount = value_column[dha_index]
        epa_index = [ i for i in range(len(name_column)) if name_column[i] == 'EPA (C20:5)' ][0]
        epa_amount = value_column[epa_index]
        if dha_amount == 0.0 and epa_amount == 0.0:
            while True:
                reply = input('Finns Omega-3 angivet? [J(a)/N(ej)] {n}: ').lower()
                if reply == 'n' or reply == '':
                    break
                if reply == 'j':
                    value_column[dha_index] = self._numeric_input('Ange Omega-3 mängd i gram: ')
                    break

        # Vitamin A/retinol calculation
        # Vitamin A has the unit "Retinol equivalent" meaning in µg. What can happen is
        # that a product has only implicit sourced of vitamin A included, such as amounts
        # of retinol and β-carotene. In principle this could be calculated (see below),
        # but, since there seems to be both problems with under- (deficencies) and over-
        # consumption (toxicity) in relation to Vitamin A, retinol, and β-carotene, we
        # will not use this calculation to add further error to these values than already
        # present in the nutrition information of the product.
        vita_index = [ i for i in range(len(name_column)) if name_column[i] == 'Vitamin A' ][0]
        vita_amount = value_column[vita_index]
        beta_index = [ i for i in range(len(name_column)) if name_column[i] == 'β-Karoten' ][0]
        beta_amount = value_column[beta_index]
        ret_index = [ i for i in range(len(name_column)) if name_column[i] == 'Retinol' ][0]
        ret_amount = value_column[ret_index]
        if vita_amount == 0 and (ret_amount != 0 or beta_amount != 0):
            # "Nordic Nutrition Recommendations 2012: Integrating nutrition and physical activity"
            # mentions that β-carotene is a large class of compunds, and the effective retinol
            # value varies depending on type, from "dietary β-carotene" which for every 12µg counts
            # as 1µg retinol, and "supplemental β-carotene" where 2µg counts as 1µg retinol, meaning
            # beta_to_ret_factor would vary from 1/2 to 1/12. To prevent accidental deficency use 1/12
            # to prevent accidental toxicity use 1/2.
            # beta_to_ret_factor = 1/12
            # value_column[vita_index] = ret_amount + beta_to_ret_factor * beta_amount
            pass

        # Energy calculation
        # If some nutritional table contains energy in kJ or kcal only, we can calculate the other
        kj_index = [ i for i in range(len(name_column)) if name_column[i] == 'Energi (kJ)' ][0]
        kj_amount = value_column[kj_index]
        kcal_index = [ i for i in range(len(name_column)) if name_column[i] == 'Energi (kcal)' ][0]
        kcal_amount = value_column[kcal_index]
        if kj_amount == 0 and kcal_amount != 0:
            value_column[kj_index] = kcal_amount * self.kcal_to_MJ*1000
        if kj_amount != 0 and kcal_amount == 0:
            value_column[kcal_index] = kj_amount/1000 * self.MJ_to_kcal

        value_column = list(map(lambda x: round(x, 2), value_column))

        return name_column, abbreviation_column, value_column, unit_column

    def _interactively_get_new_nutrition_frame(self, correct=True):
        name_column, abbreviation_column, value_column, unit_column = self._interactive_nutrition_frame()
        if correct:
            name_column, abbreviation_column, value_column, unit_column = self._interactively_correct_nutrition_columns(name_column, abbreviation_column, value_column, unit_column)

        changed_column = [ datetime.datetime.utcnow() for _ in range(len(name_column)) ]

        return NutritionDataFrame(NutritionDataFrame({
            'Namn': name_column,
            'Forkortning': abbreviation_column,
            'Varde': value_column,
            'Enhet': unit_column,
            'SenastAndrad': changed_column
        }).sort_values('Namn'))

    def interactively_add_victual(self):
        new_dataframe_row = self.dataframe._interactively_get_new_victual_row()
        new_nutrition_dataframe = self._interactively_get_new_nutrition_frame()

        print(new_nutrition_dataframe[new_nutrition_dataframe['Varde'] != 0])
        reply = input('In ["q" avslutar, allt annat godkänner] {godkänn}: ')
        if reply == 'q':
            return None

        self.add_victual(new_dataframe_row, new_nutrition_dataframe)

    def add_victual(self, new_dataframe_row, new_nutrition_dataframe):
        self.dataframe = VictualDataFrame(self.dataframe.append(new_dataframe_row, ignore_index=True))
        self.dictionary[new_dataframe_row['Nummer']] = new_nutrition_dataframe

        self._verify_types()
        self.saved = False

    def set_new_name(self, victual_number, victual_name):
        self.dataframe.set_new_name(victual_number, victual_name)
        self.saved = False

    def interactively_set_new_name(self, victual_number):
        name = self.dataframe.interactively_set_new_name(victual_number)
        self.saved = False
        return name

    def interactively_change_nutritional_frame(self, victual_nr):
        ignore_zero = None
        while True:
            reply = input('Ändra nollskilda siffror endast [(J)a/(N)ej] {n}: ').lower()
            if reply == 'n' or reply == '':
                ignore_zero = False
                break
            if reply == 'j':
                ignore_zero = True
                break
        name_column, abbreviation_column, value_column, unit_column = self._interactively_enter_nutrition_frame(start_values=self.dictionary[victual_nr], ignore_zero=ignore_zero)

        if value_column == self.dictionary[victual_nr]['Varde'].values.tolist():
            input('Inget värde förändrat [*Enter* för att fortsätta]: ')
            return None

        changed_column = [ datetime.datetime.utcnow() for _ in range(len(name_column)) ]

        new_nutrition_dataframe = NutritionDataFrame(NutritionDataFrame({
            'Namn': name_column,
            'Forkortning': abbreviation_column,
            'Varde': value_column,
            'Enhet': unit_column,
            'SenastAndrad': changed_column
        }).sort_values('Namn'))

        print(new_nutrition_dataframe[new_nutrition_dataframe['Varde'] != 0])
        reply = input('Ovan finns den nya näringsvärdelstabellen (nollskilda värden endast) ["q" avslutar, allt annat godkänner] {godkänn}: ')
        if reply == 'q':
            return None

        self.dictionary[victual_nr] = new_nutrition_dataframe
        self.saved = False

        return new_nutrition_dataframe

    def remove_victual(self, nr):
        del self.dictionary[nr]
        self.dataframe = VictualDataFrame(self.dataframe[self.dataframe['Nummer'] != nr])
        self.saved = False

    def interactively_remove_victual(self, nr):
        name = self.dataframe[self.dataframe['Nummer'] == nr]['Namn'].values[0]
        while True:
            reply = input(f'Radera "{name}"'+' ["F" för att fortsätta, "q" för att avsluta] {q}: ').lower()
            if reply == 'q' or reply == '':
                return None
            if reply == 'f':
                self.remove_victual(nr)
                break
            self.unknown_reply()

    def _interactively_get_new_victual_row(self, name=None):
        return self.dataframe._interactively_get_new_victual_row(name)

    def save(self):

        self.dataframe.to_csv(self.storage_file_basename + '_dataframe.csv', index=False)

        with open(self.storage_file_basename + '_dictionary.pickle', 'wb') as f:
            pickle.dump({number: nutritional_frame.to_csv(index=False) for number, nutritional_frame in self.dictionary.items()}, f)

        self.saved = True

    def load(self):
        if os.path.isfile(self.storage_file_basename + '_dataframe.csv'):
            self.dataframe = VictualDataFrame(pd.read_csv(self.storage_file_basename + '_dataframe.csv'))
        else:
            return False
        if os.path.isfile(self.storage_file_basename + '_dictionary.pickle'):
            with open(self.storage_file_basename + '_dictionary.pickle', 'rb') as f:
                saved_dict = pickle.load(f)
            # TODO: This takes a long time (~7s or so). IF this could be sped up, that be great.
            self.dictionary = {
                number: NutritionDataFrame(pd.read_csv(io.StringIO(csv), keep_default_na=False, parse_dates=['SenastAndrad']), clean_frame=False)
            for number, csv in saved_dict.items()}
        else:
            return False
        return True

#
#                              "
#        m mm   mmm    mmm   mmm    mmmm    mmm    mmm
#        #"  " #"  #  #"  "    #    #" "#  #"  #  #
#        #     #""""  #        #    #   #  #""""   """m
#        #     "#mm"  "#mm"  mm#mm  ##m#"  "#mm"  "mmm"
#                                   #
#                                   "
#

class Recipes(CombinedData):
    def __init__(self, dataframe=None, dictionary=None, rec_dictionary=None, storage_dir=None, verbose=False, load=True):

        self.saved = False

        self.verbose = verbose

        if storage_dir is None:
            self.storage_dir = os.environ['HOME'] + '/.livsmedel'
        else:
            self.storage_dir = storage_dir

        if not os.path.isdir(self.storage_dir):
            os.mkdir(self.storage_dir)

        self.storage_file = self.storage_dir + '/recipes.pickle'
        self.storage_file_basename = self.storage_dir + '/recipes'

        self.dataframe = None
        self.dictionary = None
        self.rec_dictionary = None
        if all(map(lambda x: x is None, (dataframe, dictionary, rec_dictionary))):
            if load:
                self.verbose and print('Recipes: Loading data from file...')
                loaded = self.load()
                if loaded:
                    dataframe = self.dataframe
                    dictionary = self.dictionary
                    rec_dictionary = self.rec_dictionary
                    self.verbose and print(f'Finished loading recipes.')
                    self.saved = True
                else:
                    self.verbose and print(f'Could not load recipes.')
        if dataframe is None:
            dataframe = RecipeDataFrame({
                'Nummer': pd.Series([], dtype='int'),
                'Namn': pd.Series([], dtype='str'),
                'Beskrivning': pd.Series([], dtype='str')
            })
        if dictionary is None:
            dictionary = dict()
        if rec_dictionary is None:
            rec_dictionary = dict()

        super().__init__(dataframe, dictionary, verbose=verbose)
        self.rec_dictionary = rec_dictionary

        self._verify_types()

    def __str__(self):
        return self.dataframe.__str__()

    def _verify_types(self):
        if not isinstance(self.dataframe, RecipeDataFrame):
            TypeError('The dataframe `self.dataframe` must be a "RecipeDataFrame".')
        self._verify_dictionary_type()

    def _verify_dictionary_type(self):
        all_vic_df = all((
            isinstance(df, VictualDataFrame)
        for key, df in self.rec_dictionary.items()))
        if not all_vic_df:
            raise TypeError('The values of the `self.rec_dictionary` dictionary must all be "VictualDataFrame"s.')

    def print_long(self, nr):
        print(f'Namn: {self.dataframe[self.dataframe["Nummer"] == nr]["Namn"].values[0]}')
        print()
        print(f'Beskrivning: {self.dataframe[self.dataframe["Nummer"] == nr]["Beskrivning"].values[0]}')
        print()
        print('Innehållstabell:')
        print(self.rec_dictionary[nr])

    def interactively_add_recipe(self, victuals):
        new_dataframe_row = self.dataframe.interactively_get_new_recipe_row()
        if new_dataframe_row is None:
            return None
        new_victual_dataframe = victuals.interactively_get_new_victual_frame()
        if new_victual_dataframe is None:
            return None
        new_nutritional_dataframe = None
        for number in new_victual_dataframe['Nummer'].values:
            fac = float(new_victual_dataframe[new_victual_dataframe['Nummer'] == number]['ViktGram'].values/100)
            tmp_frame = victuals.dictionary[number] * fac
            new_nutritional_dataframe = tmp_frame + new_nutritional_dataframe
        self.dataframe = RecipeDataFrame(self.dataframe.append(new_dataframe_row, ignore_index=True))
        self.rec_dictionary[new_dataframe_row['Nummer']] = new_victual_dataframe
        self.dictionary[new_dataframe_row['Nummer']] = new_nutritional_dataframe
        self._verify_types()
        self.saved = False

    def _interactively_pick_recipe(self):
        while True:
            print(self.dataframe)
            print('')
            reply = input('Välj recept efter nummer ["q" för att avsluta] {q}: ').lower()
            if reply == 'q' or reply == '':
                return None
            try:
                reply = int(reply)
                if reply in self.dataframe['Nummer'].values:
                    return reply
                else:
                    print('Nummer har inte ett motsvarande recept.')
            except ValueError:
                pass
            self.unknown_reply()

    def get_nutrition_table_from_recipe_list(self, recipe_list):
        nutrition_table = None
        for (rec, scale) in recipe_list:
            addition = (self.dictionary[rec] * scale)
            self.verbose and print(f"Adding {addition[addition['Namn'] == 'Energi (kcal)']['Varde'].values[0]} kcal to recipe from {self.dataframe[self.dataframe['Nummer'] == rec]['Namn'].values[0]}")
            nutrition_table = addition + nutrition_table
        self.verbose and nutrition_table is not None and print(f"Total energy for recipe list {nutrition_table[nutrition_table['Namn'] == 'Energi (kcal)']['Varde'].values[0]} kcal")
        return nutrition_table

    def interactively_get_recipe_list(self, recipe_list=None):
        if recipe_list is None:
            recipe_list = []
        else:
            recipe_numbers, _ = map(set, zip(*recipe_list))
            recipe_list = [ tup for tup in [ (number, sum(rtup[1] for rtup in recipe_list if rtup[0] == number)) for number in recipe_numbers ] if tup[1] > 0]
        while True:
            print(f'Nuvarande receptlista: {recipe_list}')
            rec_num = self._interactively_pick_recipe()
            if rec_num is None:
                break
            print(self.rec_dictionary[rec_num])
            print('')
            print('En negativ faktor (eller noll) tar bort receptet ur listan.')
            rec_scale = self._numeric_input('Ange en faktor för att skala om receptet {1.0}: ', default=1.0)
            rec_share = self._numeric_input('Ange andel av receptet du åt {1.0}: ', default=1.0)
            scale = round(rec_scale*rec_share, 2)
            recipe_list.append((rec_num, scale))

            recipe_numbers, _ = map(set, zip(*recipe_list))
            recipe_list = [ tup for tup in [ (number, sum(rtup[1] for rtup in recipe_list if rtup[0] == number)) for number in recipe_numbers ] if tup[1] > 0]

        nutrition_table = self.get_nutrition_table_from_recipe_list(recipe_list)

        return recipe_list, nutrition_table

    def get_recipes_with_victual(self, nr):
        """
        Given a victuals number, this returns a dataframe with all recipes containing that
        victual.
        """
        rec_nrs = [ rec_nr for rec_nr in self.rec_dictionary.keys() if nr in self.rec_dictionary[rec_nr]['Nummer'].values ]
        return self.dataframe[self.dataframe['Nummer'].map(lambda name: name in rec_nrs)]

    def remove_recipe(self, nr):
        del self.dictionary[nr]
        del self.rec_dictionary[nr]
        self.dataframe = RecipeDataFrame(self.dataframe[self.dataframe['Nummer'] != nr])
        self.saved = False

    def interactively_remove_recipe(self, nr):
        if isinstance(nr, int):
            if nr not in self.dataframe['Nummer'].values:
                raise ValueError(f'Provided number "{nr}" not a recipe.')
        print('\nRadera:')
        print(self.dataframe[self.dataframe['Nummer'] == nr])
        print('Samt tillhörande näringsvärdestabell och innehållsförteckning?')
        while True:
            reply = input('In ["F" för att radera, "q" för att avsluta] {q}: ').lower()
            if reply == 'q' or reply == '':
                return False
            if reply == 'f':
                break
            self.unknown_reply()
        self.remove_recipe(nr)
        return True

    def interactively_remove_recipes(self, nrs):
        if not hasattr(nrs, '__iter__'):
            raise TypeError(f'Cannot iterate over input `nrs`.')
        for nr in nrs:
            cont = self.interactively_remove_recipe(nr)
            if not cont:
                return False
        return True

    def set_new_name(self, recipe_number, recipe_name):
        self.dataframe.set_new_name(recipe_number, recipe_name)
        self.saved = False

    def interactively_set_new_name(self, recipe_number):
        name = self.dataframe.interactively_set_new_name(recipe_number)
        self.saved = False
        return name

    def set_new_description(self, recipe_number, recipe_description):
        self.dataframe.set_new_description(recipe_number, recipe_description)
        self.saved = False

    def interactively_set_new_description(self, recipe_number):
        description = self.dataframe.interactively_set_new_description(recipe_number)
        self.saved = False
        return description

    def save(self):

        self.dataframe.to_csv(self.storage_file_basename + '_dataframe.csv', index=False)

        with open(self.storage_file_basename + '_dictionary.pickle', 'wb') as f:
            pickle.dump({number: nutritional_frame.to_csv(index=False) for number, nutritional_frame in self.dictionary.items()}, f)

        with open(self.storage_file_basename + '_rec_dictionary.pickle', 'wb') as f:
            pickle.dump({number: victual_frame.to_csv(index=False) for number, victual_frame in self.rec_dictionary.items()}, f)

        self.saved = True

    def load(self):
        if os.path.isfile(self.storage_file_basename + '_dataframe.csv'):
            self.dataframe = RecipeDataFrame(pd.read_csv(self.storage_file_basename + '_dataframe.csv'))
        else:
            return False
        if os.path.isfile(self.storage_file_basename + '_dictionary.pickle'):
            with open(self.storage_file_basename + '_dictionary.pickle', 'rb') as f:
                saved_dict = pickle.load(f)
            self.dictionary = {number: NutritionDataFrame(pd.read_csv(io.StringIO(csv))) for number, csv in saved_dict.items()}
        else:
            return False
        if os.path.isfile(self.storage_file_basename + '_rec_dictionary.pickle'):
            with open(self.storage_file_basename + '_rec_dictionary.pickle', 'rb') as f:
                saved_dict = pickle.load(f)
            self.rec_dictionary = {number: VictualDataFrame(pd.read_csv(io.StringIO(csv))) for number, csv in saved_dict.items()}
        else:
            return False
        return True

#
#        m   m   mmm    mmm    m mm   mmm
#        #   #  #   "  #"  #   #"  " #   "
#        #   #   """m  #""""   #      """m
#        "mm"#  "mmm"  "#mm"   #     "mmm"
#

class User(CombinedData):
    def __init__(self, dataframe=None, dictionary=None, profile=None, name=None, verbose=False):

        self.saved = False

        self.verbose = verbose

        self.storage_dir = os.environ['HOME'] + '/.livsmedel'
        self.storage_file = None

        if name is not None:
            self.verbose and print(f'Loading user "{name}"...')
            # if name is set, we try and load from file
            self.storage_file_basename = self.storage_dir + '/user_' + hashlib.md5(bytes(name, 'UTF-8')).hexdigest()

            loaded = self.load()
            if loaded:
                dataframe = self.dataframe
                dictionary = self.dictionary
                profile = self.profile
                self.verbose and print(f'Finished loading user "{name}".')
                self.saved = True
            else:
                self.verbose and print(f'Could not load user "{name}".')

        # If no profile has been provided, initialize one now
        if profile is None:
            self.verbose and print(f'No profile given, prompting to add one.')
            if name is not None:
                self.profile = UserProfile(name=name)
            else:
                self.profile = UserProfile()
        else:
            self.profile = profile
        name = self.profile.name
        self.storage_file_basename = self.storage_dir + '/user_' + hashlib.md5(bytes(name, 'UTF-8')).hexdigest()

        if dataframe is None:
            dataframe = UserDataFrame({
                'Nummer': pd.Series([], dtype='int'),
                'Datum': pd.Series([], dtype='datetime64[ns]'),
                'Receptlista': pd.Series([], dtype='object'),
                'Vikt': pd.Series([], dtype='float'),
                'Aktivitet': pd.Series([], dtype='object')
            })
        if dictionary is None:
            dictionary = dict()

        super().__init__(dataframe=dataframe, dictionary=dictionary, verbose=verbose)

        self._verify_types()

    def __str__(self):
        return self.profile.__str__()

    def _verify_types(self):
        if not isinstance(self.dataframe, UserDataFrame):
            raise TypeError('The dataframe `self.dataframe` must be a "UserDataFrame".')
        if not isinstance(self.profile, UserProfile):
            raise TypeError('The profile `self.profile` must be a "UserProfile".')

    def save(self):
        with open(self.storage_file_basename + '_profile.txt', 'w') as f:
            f.write(self.profile.__repr__() + '\n')

        self.dataframe.to_csv(self.storage_file_basename + '_dataframe.csv', index=False)

        with open(self.storage_file_basename + '_dictionary.pickle', 'wb') as f:
            pickle.dump({number: nutritional_frame.to_csv(index=False) for number, nutritional_frame in self.dictionary.items()}, f)

        self.saved = True

    def load(self):
        if os.path.isfile(self.storage_file_basename + '_profile.txt'):
            with open(self.storage_file_basename + '_profile.txt', 'r') as f:
                profile = f.readlines()[0]
            self.profile = eval(profile)
        else:
            return False
        if os.path.isfile(self.storage_file_basename + '_dataframe.csv'):
            self.dataframe = UserDataFrame(pd.read_csv(self.storage_file_basename + '_dataframe.csv'))
        else:
            return False
        if os.path.isfile(self.storage_file_basename + '_dictionary.pickle'):
            with open(self.storage_file_basename + '_dictionary.pickle', 'rb') as f:
                saved_dict = pickle.load(f)
            self.dictionary = {number: NutritionDataFrame(pd.read_csv(io.StringIO(csv))) for number, csv in saved_dict.items()}
        else:
            return False
        return True

    def purge(self):
        if os.path.isfile(self.storage_file_basename + '_profile.txt'):
            os.remove(self.storage_file_basename + '_profile.txt')
        if os.path.isfile(self.storage_file_basename + '_dataframe.csv'):
            os.remove(self.storage_file_basename + '_dataframe.csv')
        if os.path.isfile(self.storage_file_basename + '_dictionary.pickle'):
            os.remove(self.storage_file_basename + '_dictionary.pickle')
        self.saved = False

    def update(self):
        last_date = self.dataframe['Datum'].max()

        activity = eval(self.dataframe[self.dataframe['Datum'] == last_date]['Aktivitet'].values[0])
        weight = self.dataframe[self.dataframe['Datum'] == last_date]['Vikt'].values[0]
        number = self.dataframe[self.dataframe['Datum'] == last_date]['Nummer'].values[0]

        df = self.dictionary[number]

        cei = df[df['Namn'] == 'Energi (kcal)']['Varde'].values[0]

        self.profile.update_other_attributes(cei=cei, activity=activity, weight=weight)

    def interactively_modify(self):
        return self.profile.interactively_modify()

    def interactively_add_new_user_line(self, date, recipe_list, nutrition_table):
        number = self.dataframe['Nummer'].max()
        number = 1 if np.isnan(number) else number + 1
        weight = self._numeric_input(f'Ange {self.profile.name}s vikt för datumet {pd.to_datetime(date).date()}: ')
        activity = self.profile.activity_input()

        self.dataframe = UserDataFrame(
            UserDataFrame(self.dataframe.append({
                'Nummer': number,
                'Datum': date,
                'Receptlista': str(recipe_list),
                'Vikt': weight,
                'Aktivitet': str(activity)
            }, ignore_index=True)).sort_values('Datum')
        )
        self.dictionary[number] = nutrition_table

        self.verbose and print('New line added to user.')
        if all(self.dataframe['Datum'] <= date):
            # if this entry is the latest date, update profile
            self.profile.weight = weight
            self.profile.activity = activity
            self.profile._derived_attributes()
            self.verbose and print('User-profile updated.')

    def set_verbose(self, verbose):
        self.verbose = verbose

    def print_latest(self):
        latest = self.dataframe.sort_values('Datum')[:10]
        print(latest)
        return latest

    def interactively_select_day(self):
        """
        Prompts user to select a date from the last few dates, and returns that date.
        """
        while True:
            latest = self.print_latest()
            reply = input('Välj dag ovan efter *Datum* eller *Nummer* ["q" avslutar] {q}: ').lower()
            if reply == 'q' or reply == '':
                return None
            try:
                date = pd.to_datetime(reply)
            except ValueError:
                try:
                    nr = int(reply)
                    date = pd.to_datetime(latest[latest['Nummer'] == nr]['Datum'].values[0])
                except (IndexError, ValueError):
                    self.unknown_reply()
                    continue
            if date not in pd.to_datetime(latest['Datum']).values:
                self.unknown_reply()
                continue
            else:
                break
        return date

    def interactively_modify_day(self, recipes, date):
        """
        Prompts the user to change the date entry. Alternatives to change:

            1. Change recipe list
            2. Change weight
            3. Change activity
        """
        while True:
            print(self.dataframe[pd.to_datetime(self.dataframe['Datum']) == date])
            s =  '\n'
            s += 'Välj fält att ändra:\n'
            s += '\n'
            s += '    1. Ändra receptlistan\n'
            s += '    2. Ändra vikt\n'
            s += '    3. Ändra aktivitetsnivå\n'
            s += '\n'
            s += 'In [1-3, "q" för att avsluta] {q}: '
            reply = input(s).lower()
            if reply == '' or reply == 'q':
                return None
            try:
                nr = int(reply)
            except ValueError:
                self.unknown_reply()
                continue
            if nr == 1:
                old_recipe_list = self.dataframe[pd.to_datetime(self.dataframe['Datum']) == date]['Receptlista'].values[0]
                new_recipe_list, new_nutritional_table = recipes.interactively_get_recipe_list(
                    eval(old_recipe_list)
                )
                if old_recipe_list != new_recipe_list:
                    self.dataframe.loc[pd.to_datetime(self.dataframe['Datum']) == date, 'Receptlista'] = str(new_recipe_list)
                    self.dictionary[self.dataframe.loc[pd.to_datetime(self.dataframe['Datum']) == date, 'Nummer'].values[0]] = new_nutritional_table
                    self.saved = False
                continue
            elif nr == 2:
                old_weight = self.dataframe.loc[pd.to_datetime(self.dataframe['Datum']) == date, 'Vikt'].values[0]
                weight = self._numeric_input('Ange ny vikt {' + str(old_weight) + '}: ')
                if weight != old_weight:
                    self.dataframe.loc[pd.to_datetime(self.dataframe['Datum']) == date, 'Vikt'] = weight
                    self.saved = False
                continue
            elif nr == 3:
                old_activity = self.dataframe.loc[pd.to_datetime(self.dataframe['Datum']) == date, 'Aktivitet'].values[0]
                print(f'Den gamla aktivitetsnivån: {old_activity}')
                activity = self.profile.activity_input()
                if eval(old_activity) != activity:
                    self.dataframe.loc[pd.to_datetime(self.dataframe['Datum']) == date, 'Aktivitet'] = str(activity)
                    self.saved = False
                continue
            else:
                self.unknown_reply()
                continue

    def interactively_remove_day(self, date):
        while True:
            reply = input(f'Du har valt att ta bort datum {date}. ' + 'Godkänn [J(a)/N(ej)] {j}: ').lower()
            if reply == 'j' or reply == '':
                self.dataframe = self.dataframe.drop_date(pd.to_datetime(date))
                self.saved = False
                return True
            if reply == 'n':
                return False
            self.unknown_reply()

class Users():
    def __init__(self, user_list=None, storage_dir=None, verbose=False, load=True):

        self.verbose = verbose

        self.saved = False

        if storage_dir is None:
            self.storage_dir = os.environ['HOME'] + '/.livsmedel'
        else:
            self.storage_dir = storage_dir

        if not os.path.isdir(self.storage_dir):
            os.mkdir(self.storage_dir)

        self.storage_file = self.storage_dir + '/users.txt'

        if user_list is not None:
            if os.path.isfile(self.storage_file):
                print('WARN: Storage file exists, stored data may be overwritten.')
            self.user_list = user_list
        elif os.path.isfile(self.storage_file) and load:
            self.verbose and print('Loading data from file...')
            loaded = self.load()
            if not loaded:
                self.user_list = []
            else:
                self.saved = True
        else:
            self.verbose and print('Users: Creating default user_list.')
            self.user_list = []

        self.purge_list = []

        self._verify_types()

        for user in self.user_list:
            user.set_verbose(self.verbose)

    def __len__(self):
        return len(self.user_list)

    def _verify_types(self):
        for u in self.user_list:
            if not isinstance(u, User):
                raise TypeError('User in provided user list is not of type User.')

    def save(self):
        self.verbose and print('Users: Saving data to file...')
        name_list = []
        for user in self.user_list:
            name_list.append(user.profile.name)
            user.save()
        with open(self.storage_file, 'w') as f:
            f.write(str(name_list) + '\n')
        for user in self.purge_list:
            user.purge()
        self.purge_list = []
        self.saved = True

    def load(self):
        if os.path.isfile(self.storage_file):
            with open(self.storage_file, 'r') as f:
                name_list = eval(f.readlines()[0])
            self.user_list = [ User(name=name, verbose=self.verbose) for name in name_list ]
        else:
            return False
        return True

    def append(self, user):
        if not isinstance(user, User):
            raise TypeError(f'Cannot add user. Expected "User" got {type(user)}')
        self.user_list.append(user)
        self.saved = False

    def remove(self, nr_or_name, purge=False, delayed_purge=True):
        if isinstance(nr_or_name, int):
            idx = nr_or_name
        elif isinstance(nr_or_name, str):
            idx = [ user.profile.name for user in self.user_list ].index(nr_or_name)

        if purge:
            self.user_list[idx].purge()
        if delayed_purge and not purge:
            self.purge_list.append(self.user_list[idx])
        del self.user_list[idx]

        self.saved = False

    def unknown_reply(self):
        print('Inmatad data ej förstådd. Försök igen.')

    def interactively_modify(self, nr):
        modified = self.user_list[nr].interactively_modify()
        if modified:
            self.saved = False

    def interactively_add_new_user_line(self, nr, date, recipe_list, nutrition_table):
        self.user_list[nr].interactively_add_new_user_line(date, recipe_list, nutrition_table)
        self.saved = False

    def print_latest(self, nr):
        self.user_list[nr].print_latest()

    def interactively_select_day(self, nr):
        return self.user_list[nr].interactively_select_day()

    def interactively_modify_day(self, nr, recipes, date=None):
        if date is None:
            date = self.user_list[nr].interactively_select_day()
            if date is None:
                return None
        self.user_list[nr].interactively_modify_day(recipes, date)
        if not self.user_list[nr].saved:
            self.saved = False

    def interactively_remove_day(self, nr, date):
        removed = self.user_list[nr].interactively_remove_day(date)
        if removed:
            self.saved = False
        return removed

    def interactively_select_user(self):
        """
        Prompts user to select a user. Possible returns are:

            * None: No registered users.
            * 'q': User wish to return, not selecting a user.
            * int(): index for user in Users.user_list
        """
        if len(self) == 0:
            reply = input("""
    Inga användare registrerade.

In [*Enter* för att återvända]: """)
            return None
        else:
            message_string = 'Välj användare: \n'
            message_string += '\n'
            nr = 1
            for u in self.user_list:
                message_string += f'    {nr}. {u.profile.name}\n'
                nr += 1
            message_string += '\n'
            message_string += 'In [ange nummer, "q" för att återvända] {q}: '

            while True:
                reply = input(message_string).lower()
                try:
                    reply = int(reply)
                    if reply < 0 or reply > len(self)+1:
                        self.unknown_reply()
                    else:
                        nr = reply
                        return nr-1
                except ValueError:
                    if reply == 'q' or reply == '':
                        return 'q'
                    self.unknown_reply()
