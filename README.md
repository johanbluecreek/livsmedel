# Livsmedel

## Installation

För att installera *Livsmedel*

```bash
$ git clone https://gitlab.com/johanbluecreek/livsmedel.git
```

se sedan filen `requirements.txt` för de Pythonpaket som behöver vara installerade.

## Användande

För att starta programmet, kör

```bash
$ ./main.py
```

Detta ger dig en interaktiv form av programmet. See även paragrafen [Varning](https://gitlab.com/johanbluecreek/livsmedel#varning).

## Källa

Denna programvara använder sig av data hämtat från [Livsmedelsverkets livsmedelsdatabas](https://www.livsmedelsverket.se/om-oss/psidata/livsmedelsdatabasen) (version 20210101, eller version angiven av användaren). Databasen hämtas, behandlas, och sparas utav programvaran (under normalt användande). Databasen är ej inkluderad, och delas inte, tillsammans med programvaran. [Livsmedelsverkets livsmedelsdatabas](https://www.livsmedelsverket.se/om-oss/psidata/livsmedelsdatabasen#Licens) är publicerad under licensen [Creative Commons Erkännande 4.0 (PDF)](http://www.creativecommons.se/wp-content/uploads/2015/01/CreativeCommons-Erk%C3%A4nnande-4.0.pdf).

Modeller och förslag som används eller på annat sätt beräknas av programvaran är delvis tolkningar (see paragrafen [Varning](https://gitlab.com/johanbluecreek/livsmedel#varning)) av resultaten som finns i ["Nordic Nutrition Recommendations 2012: Integrating nutrition and physical activity"](http://norden.diva-portal.org/smash/record.jsf?pid=diva2%3A704251&dswid=-6997).

## Varning

Denna programvara garanterar **inte** korrekt eller pålitliga beräknade resultat, och är **inte** stödd, godkänd, eller framställd av någon organisation eller person med relevant akademisk eller professionell anslutning, bakgrund, eller utbildning. Beräkningar som utförs av denna programvara och görs tillgängliga för användaren ska **inte** ses som hälsorådgivning eller kostförslag. Använd **inte** något resultat eller data presenterat av denna programvara utan att rådfråga en läkare först.

## TODO

  * Add option to add recipe when adding a new day to a user
  * Add option to add victual entry when adding new recipe
  * Add possibility to remove victuals entry
    - make sure it is not present in a recipe
  * Add possibility to remove recipes
    - Make sure recipes are not removed if they have been added to a users day.
  * Make it possible to copy victual entries
  * Make it possible to import a recipe as a victual. For example, you may have
    a recipe for bread, and want to make a recipe for your breakfast. Import the bread
    as a victual, and you can add it to a new recipe.

  * Cannot erase a Swedish character. input dies with `UnicodeDecodeError: 'utf-8' codec can't decode byte 0xc3 in position 0: unexpected end of data`.
