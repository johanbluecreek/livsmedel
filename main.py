#!/usr/bin/env python3

import os, sys
import pandas as pd
from livsmedel.interactive import InteractiveSession

if 'test' in sys.argv:
    pd.set_option('display.max_rows', 0)
    pd.set_option('display.max_columns', 0)
    pd.set_option('display.width', 0)
    pd.set_option('display.max_colwidth', 0)

if 'debug' in sys.argv:
    import IPython

def main():
    int_sess = InteractiveSession(verbose=True)
    while not int_sess.done:
        int_sess.current()
    # The interactive session may have threads running, if `done`, we can kill it
    # anyway.
    if 'debug' in sys.argv:
        IPython.embed()
    else:
        os._exit(0)

if __name__ == '__main__':
    main()
