import os, shutil

def secure_test(fn):
    """
    Moves the working directory to a backup directory before executing the test. The
    backup will be restored after the test. A test does not need to restore the
    state of the working directory through interactions, and can just rely on the
    backup instead.
    """
    def wrap(*args):
        storage_dir = os.environ['HOME'] + '/.livsmedel'
        backup_dir = os.environ['HOME'] + '/.livsmedel_backup'
        # backup
        if os.path.isdir(backup_dir):
            shutil.rmtree(backup_dir)
        shutil.copytree(storage_dir, backup_dir)
        # execute
        e = None
        try:
            fn(*args)
        except Exception as error:
            e = error
            pass
        # restore backup
        if os.path.isdir(backup_dir) and os.path.isdir(backup_dir):
            # only remove the storage_dir if the backup_dir exists
            # just not to lose everything
            shutil.rmtree(storage_dir)
        shutil.copytree(backup_dir, storage_dir)
        if os.path.isdir(backup_dir) and os.path.isdir(backup_dir):
            # if backup_dir has been restored, we can delete it
            shutil.rmtree(backup_dir)
        if e is not None:
            raise e

    return wrap
