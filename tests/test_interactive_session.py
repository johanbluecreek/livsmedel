
# python3 -m unittest tests/test_interactive_session.py

import unittest

from tests.utils import secure_test

import subprocess, random

def execute_interaction(interaction):
    with subprocess.Popen(('echo', '-e', interaction + 'n\n'), stdout=subprocess.PIPE) as process:
        with subprocess.Popen(('./main.py', 'test'), stdin=process.stdout, stdout=subprocess.PIPE) as pprocess:
            output = pprocess.communicate()
            code = pprocess.returncode
            return code, output
    return -1, (b'',)

def detailed_failure(error, output):
    print('Failure. Printing interaction:')
    print(output[0].decode().replace(chr(27) + "[2J",''))
    print('Interaction complete. Rasing error.')
    raise error


class TestInteractiveGeneric(unittest.TestCase):

    def test_exit(self):
        """
        Starts the interactive session and exits without any changes.
        """
        i =  ''
        i += 'q\n' # exit home
        i += 'j\n' # confirm exit
        self.assertEqual(execute_interaction(i)[0], 0)


    def test_exit_one_refusal(self):
        """
        Starts the interactive session and exits without any changes.
        """
        i =  ''
        i += 'q\n' # exit home
        i += '\n'  # save
        i += 'n\n' # deny exit
        i += 'q\n' # exit home
        i += 'j\n' # confirm exit
        self.assertEqual(execute_interaction(i)[0], 0)

    def test_help(self):
        """
        Enter help and exit.
        """
        i = ''
        i += 'h\n' # enter help
        i += '\n'  # exit help
        i += 'q\n' # exit home
        i += 'j\n' # confirm exit
        self.assertEqual(execute_interaction(i)[0], 0)

class TestInteractiveVictual(unittest.TestCase):

    def test_search_victual(self):
        """
        Enter the search menu for victuals, make a search, select and exit.
        """
        # find entry number
        i =  ''
        i += 'l\n' # enter victuals
        i += 's\n' # search victual
        i += 'Nöt talg\n' # search phrase
        i += '1\n' # select number for nutritional table
        # make sure the loop works with another search
        i += 'Nöt talg\n' # search phrase
        i += '1\n' # select number for nutritional table
        i += 'q\n' # exit to victual
        i += 'q\n' # exit victual
        i += 'q\n' # exit home
        i += 'j\n' # confirm exit
        self.assertEqual(execute_interaction(i)[0], 0)

    @secure_test
    def test_add_delete_victual_long(self):
        """
        Adds a victual entry by entering nutritional values manually (long-form),
        searches for it, exits. Then starts the interaction again delete the same entry.
        """
        # add entry
        i =  ''
        i += 'l\n' # enter victuals
        i += 'l\n' # enter add victual
        i += 'DUMMYENTRY\n' # dummy name
        i += 'l\n' # select long entry
        i += '0.0\n'*56 # 56 nutritional entries
        i += 'j\n' # yes on tryptofan
        i += '0.0\n' # zero tryptofan
        i += 'j\n' # yes on omega3
        i += '0.0\n' # zero omega3
        i += '\n' # confrim entry
        # search to find number
        i += 's\n' # search victual
        i += 'DUMMYENTRY\n' # search phrase
        i += 'q\n' # exit to victual
        # exit
        i += 'q\n' # exit victual
        i += 'q\n' # exit home
        i += 'j\n' # save victual
        i += 'j\n' # confirm exit

        code, output = execute_interaction(i)
        self.assertEqual(code, 0)

        nr = [ line for line in output[0].decode().split('\n') if 'DUMMYENTRY' in line ][0].split()[1]

        # delete entry
        i = ''
        i += 'l\n' # enter victuals
        i += 't\n' # delete victuals
        i += 'DUMMYENTRY\n' # search phrase
        i += nr + '\n' # select entry
        i += 'F\n' # confirm deletion
        i += 'q\n' # exit victual
        i += 'q\n' # exit home
        i += 'j\n' # save victual
        i += 'j\n' # confirm exit
        self.assertEqual(execute_interaction(i)[0], 0)

    @secure_test
    def test_add_victual_long_alt1(self):
        """
        Adds a victual entry by entering nutritional values manually (long-form),
        searches for it, exits.

        Alternative version where some alternative options are taken compared to the
        none '_alt' version.
        """
        # add entry
        i =  ''
        i += 'l\n' # enter victuals
        i += 'l\n' # enter add victual
        i += 'DUMMYENTRY\n' # dummy name
        i += 'l\n' # select long entry
        i += ''.join([ str(round(random.random()*10,2))+'\n' for _ in range(56) ]) # 56 nutritional entries
        i += '\n' # confrim entry
        # search to find number
        i += 's\n' # search victual
        i += 'DUMMYENTRY\n' # search phrase
        i += 'q\n' # exit to victual
        # exit
        i += 'q\n' # exit victual
        i += 'q\n' # exit home
        i += 'j\n' # save victual
        i += 'j\n' # confirm exit
        self.assertEqual(execute_interaction(i)[0], 0)

    @secure_test
    def test_add_victual_short(self):
        """
        Adds a victual entry by entering nutritional values by list (short-form),
        searches for it, exits.
        """
        # add entry
        i =  ''
        i += 'l\n' # enter victuals
        i += 'l\n' # enter add victual
        i += 'DUMMYENTRY\n' # dummy name
        i += 's\n' # select short entry
        i += '[(Energi (kcal), 643), (Fett, 56), (Summa mättade fettsyror, 6.3)]\n' # entry to add
        i += 'j\n' # yes on tryptofan
        i += '0.0\n' # zero tryptofan
        i += 'j\n' # yes on omega3
        i += '0.0\n' # zero omega3
        i += '\n' # confrim entry
        # search to find number
        i += 's\n' # search victual
        i += 'DUMMYENTRY\n' # search phrase
        i += 'q\n' # exit to victual
        # exit
        i += 'q\n' # exit victual
        i += 'q\n' # exit home
        i += 'j\n' # save victual
        i += 'j\n' # confirm exit
        self.assertEqual(execute_interaction(i)[0], 0)

    def test_bad_victual_mix_short(self):
        # add entry
        i =  ''
        i += 'l\n' # enter victuals
        i += 'm\n' # enter victual mix
        i += 'DUMMYENTRY\n' # dummy name
        i += 's\n' # select short entry
        i += '[(Energi (kcal), 643), (Fett, 56), (Summa mättade fettsyror, 6.3)]\n' # entry to add
        i += 'Valnötter\n' # add
        i += '1576\n'
        i += 'Hasselnötter\n' # add
        i += '1558\n'
        i += 'q\n' # preview
        i += 'q\n' # exit preview
        i += 'G\n' # confirm preview
        i += 'q\n' # abort because of bad entry
        i += 'q\n' # exit victual
        i += 'q\n' # exit home
        i += 'j\n' # confrim quit
        self.assertEqual(execute_interaction(i)[0], 0)

    @secure_test
    def test_add_victual_mix_short(self):
        # add mix
        i =  ''
        i += 'l\n' # enter victuals
        i += 'm\n' # enter victual mix
        i += 'DUMMYENTRY\n' # dummy name
        i += 's\n' # select short entry
        i += '[(Energi (kJ), 2661.0), (Energi (kcal), 635.0), (Fett, 54.0), (Fibrer, 14.0), (Fosfor, 561.0), (Kalcium, 181.0), (Kalium, 846.0), (Magnesium, 273.0), (Protein, 20.0), (Summa enkelomättade fettsyror, 35.0), (Summa fleromättade fettsyror, 11.0), (Summa mättade fettsyror, 6.0), (Vitamin E, 14.0), (Zink, 4.0), (Karoten, 7.0)]\n'
        i += 'Valnötter\n' # add
        i += '1576\n'
        i += 'Hasselnötter\n' # add
        i += '1558\n'
        i += 'Sötmandel\n' # add
        i += '1575\n'
        i += 'Cashewnötter rostade u. salt\n' # add
        i += '1557\n'
        i += 'q\n' # preview
        i += 'q\n' # exit preview
        i += 'g\n' # confirm preview
        i += '\n' # exit solution
        i += '\n' # exit nutritional preview
        # exit
        i += 'q\n' # exit victual
        i += 'q\n' # exit home
        i += 'j\n' # save victual
        i += 'j\n' # confirm exit
        self.assertEqual(execute_interaction(i)[0], 0)

    @secure_test
    def test_modify_victual(self):
        """
        Adds a victual entry by entering nutritional values by list (short-form),
        and then explores all ways of modifying the entry.
        """

        # Add entry to be modified

        i =  ''
        i += 'l\n' # enter victuals
        i += 'l\n' # enter add victual
        i += 'DUMMYENTRY\n' # dummy name
        i += 's\n' # select short entry
        i += '[(Energi (kcal), 643), (Fett, 56), (Summa mättade fettsyror, 6.3)]\n' # entry to add
        i += 'j\n' # yes on tryptofan
        i += '0.0\n' # zero tryptofan
        i += 'j\n' # yes on omega3
        i += '0.0\n' # zero omega3
        i += '\n' # confrim entry
        # search to find number
        i += 's\n' # search victual
        i += 'DUMMYENTRY\n' # search phrase
        i += 'q\n' # exit to victual
        # exit
        i += 'q\n' # exit victual
        i += 'q\n' # exit home
        i += 'j\n' # save victual
        i += 'j\n' # confirm exit

        code, output = execute_interaction(i)
        self.assertEqual(code, 0)

        nr = [ line for line in output[0].decode().split('\n') if 'DUMMYENTRY' in line ][0].split()[1]

        # Modify name

        i =  ''
        i += 'l\n' # enter victuals
        i += 'j\n' # enter modify menu
        i += 'DUMMYENTRY\n' # search phrase
        i += nr + '\n' # select entry
        i += 'n\n' # choose to modify name
        i += 'DUMMYENTRY2\n' # new name
        i += 'q\n' # exit modification type prompt
        i += 'q\n' # exit modify menu
        # search
        i += 's\n'
        i += 'DUMMYENTRY2\n' # search phrase
        i += 'q\n' # exit to victual
        #
        i += 'q\n' # exit victual
        i += 'q\n' # exit home
        i += 'j\n' # save victuals
        i += 'j\n'

        code, output = execute_interaction(i)
        self.assertEqual(code, 0)

        l = len([ line for line in output[0].decode().split('\n') if 'DUMMYENTRY2' in line ])
        try:
            self.assertGreater(l, 0)
        except AssertionError as e:
            detailed_failure(e, output)

        # Modify nutritional values (only non-zeroes)

        i =  ''
        i += 'l\n' # enter victuals
        i += 'j\n' # enter modify menu
        i += 'DUMMYENTRY2\n' # search phrase
        i += nr + '\n' # select entry
        i += 'm\n' # choose to modify nutritional values
        i += 'j\n' # only non-zeroes (there are 4 entries)
        i += '2690\n' # Energi (kJ)
        i += '\n'*3 # default on the rest
        i += '\n' # accept the preview table
        i += 'q\n' # exit modification type prompt
        i += 'q\n' # exit modify menu
        i += 'q\n' # exit victual
        i += 'q\n' # exit home
        i += 'j\n' # save victuals
        i += 'j\n'

        self.assertEqual(execute_interaction(i)[0], 0)

        # Modify nutritional values (including zeroes)

        i =  ''
        i += 'l\n' # enter victuals
        i += 'j\n' # enter modify menu
        i += 'DUMMYENTRY2\n' # search phrase
        i += nr + '\n' # select entry
        i += 'm\n' # choose to modify nutritional values
        i += 'j\n' # including zeroes
        i += '0.1\n' # Alkohol
        i += '\n'*55 # default on the rest
        i += '\n' # accept the preview table
        i += 'q\n' # exit modification type prompt
        i += 'q\n' # exit modify menu
        i += 'q\n' # exit victual
        i += 'q\n' # exit home
        i += 'j\n' # save victuals
        i += 'j\n'

        self.assertEqual(execute_interaction(i)[0], 0)

class TestInteractiveRecipe(unittest.TestCase):

    @secure_test
    def test_add_edit_remove_recipe(self):

        # Add recipe

        i =  ''
        i += 'r\n' # enter recipes
        i += 'l\n' # add recipe
        i += 'DUMMYENTRY\n' # name of recipe
        i += 'THIS IS A DUMMY ENTRY\n' # description of recipe
        i += 'Hasselnötter\n' # add
        i += '1558\n'
        i += '321\n'
        i += 'Saftkräm\n' # add
        i += '1746\n'
        i += '123\n'
        i += 'q\n' # preview
        i += '1746\n' # number to edit
        i += '231\n' # new weight
        i += 'q\n' # preview
        i += 'q\n' # to continue/confirm menu
        i += 'f\n' # continue
        i += 'Kalv filé rå\n' # add
        i += '936\n'
        i += '0.0\n' # do not add this mistake
        i += 'Tomat\n' # add
        i += '364\n'
        i += '765\n'
        i += 'q\n' # preview
        i += 'q\n' # to continue/confirm menu
        i += 'g\n' # confirm
        # search
        i += 's\n' # search menu
        i += 'n\n' # select search by name
        i += 'DUMMYENTRY\n' # search phrase
        # exit
        i += 'q\n' # exit search result
        i += 'q\n' # exit search menu
        i += 'q\n' # exit recipe menu
        i += 'q\n' # exit home
        i += 'j\n' # save recipes
        i += 'j\n' # confirm exit

        code, output = execute_interaction(i)
        self.assertEqual(code, 0)

        nr = [ line for line in output[0].decode().split('\n') if 'THIS IS A DUMMY ENTRY' in line ][0].split()[1]

        # Modify name

        i =  ''
        i += 'r\n' # enter recipes
        i += 'm\n' # modify recipe
        i += 'DUMMYENTRY\n' # search phrase
        i += nr + '\n' # select entry
        # change name
        i += 'n\n'
        i += 'DUMMYENTRY2\n'
        i += 'q\n' # exit modify
        # exit program
        i += 'q\n' # exit recipe
        i += 'q\n' # exit home
        i += 'j\n' # save recipes
        i += 'j\n' # confrim quit

        self.assertEqual(execute_interaction(i)[0], 0)

        # verify name is changed

        i =  ''
        i += 'r\n' # enter recipes
        i += 's\n' # search recipe
        i += 'n\n' # by name
        i += 'DUMMYENTRY2\n' # new name
        i += 'q\n' # exit search result
        i += 'q\n' # exit search menu
        i += 'q\n' # exit recipe
        i += 'q\n' # exit home
        i += 'j\n' # confrim exit

        code, output = execute_interaction(i)
        self.assertEqual(code, 0)

        l = len([ line for line in output[0].decode().split('\n') if 'DUMMYENTRY2' in line ])
        try:
            self.assertGreater(l, 0)
        except AssertionError as e:
            detailed_failure(e, output)

        # Modify description

        i =  ''
        i += 'r\n' # enter recipes
        i += 'm\n' # modify recipe
        i += 'DUMMYENTRY2\n' # search phrase
        i += nr + '\n' # select entry
        # change description
        i += 'b\n'
        i += 'THIS IS A DUMMY ENTRY 2\n'
        i += 'q\n' # exit modify
        # exit program
        i += 'q\n' # exit recipe
        i += 'q\n' # exit home
        i += 'j\n' # save recipes
        i += 'j\n' # confrim quit

        self.assertEqual(execute_interaction(i)[0], 0)

        # make sure new description is changed

        i =  ''
        i += 'r\n' # enter recipes
        i += 's\n' # search recipe
        i += 'b\n' # by description
        i += 'THIS IS A DUMMY ENTRY 2\n' # new description
        i += 'q\n' # exit search result
        i += 'q\n' # exit search menu
        i += 'q\n' # exit recipe
        i += 'q\n' # exit home
        i += 'j\n' # confrim exit

        code, output = execute_interaction(i)
        self.assertEqual(code, 0)

        l = len([ line for line in output[0].decode().split('\n') if 'THIS IS A DUMMY ENTRY 2' in line ])
        self.assertGreater(l, 0)

        # Modify ingredients

        i =  ''
        i += 'r\n' # enter recipes
        i += 'm\n' # modify recipe
        i += 'DUMMYENTRY2\n' # search phrase
        i += nr + '\n' # select entry
        # change ingredients
        i += 'i\n'
        # add
        i += 'Pitepalt\n'
        i += '253\n'
        i += '392\n'
        # change
        i += 'q\n'
        i += '364\n'
        i += '1\n'
        # remove
        i += 'q\n'
        i += '1746\n'
        i += '0.0\n'
        # stop modifying
        i += 'q\n' # preview
        i += 'q\n' # quit preview
        i += 'g\n' # confirm
        i += 'q\n' # exit modify
        # exit program
        i += 'q\n' # exit recipe
        i += 'q\n' # exit home
        i += 'j\n' # save recipes
        i += 'j\n' # confrim quit

        self.assertEqual(execute_interaction(i)[0], 0)

        # make sure new ingredient is added

        i =  ''
        i += 'r\n' # enter recipes
        i += 'a\n' # list all
        i += nr + '\n' # select entry
        i += 'q\n' # exit ingredient list
        i += 'q\n' # exit all list
        i += 'q\n' # exit recipe
        i += 'q\n' # exit home
        i += 'j\n' # confrim exit

        code, output = execute_interaction(i)
        self.assertEqual(code, 0)

        l = len([ line for line in output[0].decode().split('\n') if 'Pitepalt' in line ])
        self.assertGreater(l, 0)

        # Remove recipe

        i =  ''
        i += 'r\n' # enter recipes
        i += 't\n' # remove
        i += 'DUMMYENTRY2\n' # search
        i += nr + '\n' # select
        i += 'q\n' # do not confirm deletion
        i += 't\n' # remove
        i += 'DUMMYENTRY2\n' # search
        i += nr + '\n' # select
        i += 'f\n'
        # exit
        i += 'q\n' # exit recipe
        i += 'q\n' # exit home
        i += 'j\n' # confirm save
        i += 'j\n' # confirm exit

        self.assertEqual(execute_interaction(i)[0], 0)

class TestInteractiveUsers(unittest.TestCase):

    @secure_test
    def test_add_save_delete_user(self):
        """
        Adds and saves a user. Exits, and finds that user. Then deletes user.
        """

        # add user, save and exit

        i =  ''
        i += 'a\n' # user menu
        i += 'l\n' # user add
        i += 'DUMMY USER\n' # user name
        i += '1970-01-01\n' # dob
        i += str(random.random()*100) + '\n' # weight
        i += str(random.random()) + '\n' # height
        i += random.choice(['h', 'l', 'm', 'k']) + '\n' # extra factor
        i += 'q\n' # exit user menu
        i += 'q\n' # exit home
        i += 'j\n' # save user
        i += 'j\n' # confirm exit

        self.assertEqual(execute_interaction(i)[0], 0)

        # find user to get number

        i = ''
        i += 'a\n' # user menu
        i += 'p\n' # print profile -- has list of users
        i += 'q\n' # cancel printing profile
        i += 'q\n' # exit user menu
        i += 'q\n' # exit home
        i += 'j\n' # confirm exit

        code, output = execute_interaction(i)
        self.assertEqual(code, 0)

        user_list = [ line for line in output[0].decode().split('\n') if 'DUMMY USER' in line and 'loading' not in line.lower() ]
        l = len(user_list)
        self.assertGreater(l, 0)

        try:
            nr = int(float(user_list[0].split()[0]))
        except ValueError as e:
            detailed_failure(e, output)

        # remove user

        i = ''
        i += 'a\n' # user menu
        i += 't\n' # remove user menu
        i += str(nr) + '\n' # select for removal
        i += 'a\n' # abort
        i += '\n' # exit confirm screen
        i += str(nr) + '\n' # select for removal
        i += 't\n' # confirm remove
        i += '\n' # exit confrim screen
        i += 'q\n' # exit remove menu
        i += 'q\n' # exit user menu
        i += 'q\n' # exit home
        i += 'j\n' # save user data
        i += 'j\n' # confirm exit

        code, output = execute_interaction(i)
        try:
            self.assertEqual(code, 0)
        except AssertionError as e:
            detailed_failure(e, output)

        # verify removal

        i = ''
        i += 'a\n' # user menu
        i += 'p\n' # print profile -- has list of users
        i += 'q\n' # cancel printing profile
        i += 'q\n' # exit user menu
        i += 'q\n' # exit home
        i += 'j\n' # confirm exit

        code, output = execute_interaction(i)
        self.assertEqual(code, 0)

        user_list = [ line for line in output[0].decode().split('\n') if 'DUMMY USER' in line and 'loading' not in line.lower() ]
        l = len(user_list)

        try:
            self.assertEqual(l, 0)
        except AssertionError as e:
            detailed_failure(e, output)

    @secure_test
    def test_add_user_to_add_modify_and_remove_days(self):

        # add user, save and exit

        i =  ''
        i += 'a\n' # user menu
        i += 'l\n' # user add
        i += 'DUMMY USER\n' # user name
        i += '1970-01-01\n' # dob
        i += str(random.random()*100) + '\n' # weight
        i += str(random.random()) + '\n' # height
        i += random.choice(['h', 'l', 'm', 'k']) + '\n' # extra factor
        i += 'p\n' # print profile -- has list of users
        i += 'q\n' # cancel printing profile
        i += 'q\n' # exit user menu
        i += 'q\n' # exit home
        i += 'j\n' # save user
        i += 'j\n' # confirm exit

        code, output = execute_interaction(i)
        self.assertEqual(code, 0)

        user_list = [ line for line in output[0].decode().split('\n') if 'DUMMY USER' in line and 'loading' not in line.lower() ]
        l = len(user_list)
        self.assertGreater(l, 0)

        try:
            nr = int(float(user_list[0].split()[0]))
        except ValueError as e:
            detailed_failure(e, output)

        i =  ''

        # first, ensure two recipes are present

        i += 'r\n' # enter recipes

        i += 'l\n' # add recipe
        i += 'DUMMYENTRY\n' # name of recipe
        i += 'THIS IS A DUMMY ENTRY\n' # description of recipe
        i += 'Hasselnötter\n' # add
        i += '1558\n'
        i += '321\n'
        i += 'q\n' # preview
        i += 'q\n' # to continue/confirm menu
        i += 'g\n' # confirm

        i += 'l\n' # add recipe
        i += 'DUMMYENTRY2\n' # name of recipe
        i += 'THIS IS A DUMMY ENTRY 2\n' # description of recipe
        i += 'Kalv filé rå\n' # add
        i += '936\n'
        i += '110\n'
        i += 'q\n' # preview
        i += 'q\n' # to continue/confirm menu
        i += 'g\n' # confirm

        i += 'q\n' # exit recipe menu

        # add days for user

        i += 'a\n' # user menu
        i += 'n\n' # user add day

        i += str(nr) + '\n' # select user
        i += '1972-02-02\n' # new date
        i += '1\n' # add first recipe, whatever it is, there are at least two present
        i += '3.2\n' # scale
        i += '0.5\n' # part
        i += 'q\n' # done with the day
        i += '123\n' # weight
        i += '1\n2\n\n\n\n' # activity
        i += 'j\n' # confirm activity

        i += str(nr) + '\n' # select user
        i += '1972-02-02\n' # new date
        i += 'n\n' # abort overwrite of day
        i += '1972-02-03\n' # new date
        i += '1\n' # add first recipe, whatever it is, there are at least two present
        i += '1.2\n' # scale
        i += '2.5\n' # part
        i += 'q\n' # done with the day
        i += '122\n' # weight
        i += '2\n4\n1\n1\n1\n' # activity
        i += 'j\n' # confirm activity

        i += 'q\n'

        # start modifying days

        i += 'j\n' # modify day
        i += str(nr) + '\n' # select user
        i += '1972-02-02\n' # new date

        # modify recipe list

        i += '1\n' # recipe list
        i += '1\n' # recipe 1
        i += '7.2\n' # scale
        i += '0.2\n' # part
        i += '2\n' # recipe 2
        i += '2.1\n' # scale
        i += '\n' # default part
        i += 'q\n' # finish modifying recipe list

        # modify weight

        i += '2\n' # weight
        i += '98\n' # new weight

        # activity

        i += '3\n' # activity
        i += '0\n0\n0\n0\n0\n' # enter wrong entry
        i += 'n\n' # do not accept entry
        i += str(random.random()) + '\n' + str(random.random()) + '\n' + str(random.random()) + '\n' + str(random.random()) + '\n' + str(random.random()) + '\n' # random activity
        i += 'j\n' # accept

        i += 'q\n' # stop modifying
        i += 'q\n' # exit users select screen

        # remove day

        i += 'd\n' # delete day option
        i += str(nr) + '\n' # select user
        i += '1972-02-03\n' # date to delete
        i += 'j\n' # confirm deletion
        i += 'q\n' # exit user selection screen
        i += 'q\n' # exit user menu

        # quit

        i += 'q\n' # exit home screen
        i += 'j\n' # confirm save recipes
        i += 'j\n' # confirm save users
        i += 'j\n' # confirm exit

        code, output = execute_interaction(i)
        try:
            self.assertEqual(code, 0)
        except AssertionError as e:
            detailed_failure(e, output)

if __name__ == '__main__':
    pass
