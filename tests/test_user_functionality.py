
# python3 -m unittest tests/test_init_objects.py

import unittest

import random, os

from livsmedel.users import UserProfile
from livsmedel.combodata import User, Users

from tests.utils import secure_test

class TestUserProfile(unittest.TestCase):

    def test_init_profile_explicit(self):
        profile = UserProfile(name='DUMMY USER', dob='1970-01-01', weight=random.random()*100, height=random.random(), sex='h')
        self.assertIsInstance(profile, UserProfile)
        profile = UserProfile(name='DUMMY USER', dob='1970-01-01', weight=random.random()*100, height=random.random(), sex='l')
        self.assertIsInstance(profile, UserProfile)
        profile = UserProfile(name='DUMMY USER', dob='1970-01-01', weight=random.random()*100, height=random.random(), sex='m')
        self.assertIsInstance(profile, UserProfile)
        profile = UserProfile(name='DUMMY USER', dob='1970-01-01', weight=random.random()*100, height=random.random(), sex='k')
        self.assertIsInstance(profile, UserProfile)

class TestUser(unittest.TestCase):

    def test_init_user_with_profile(self):
        profile = UserProfile(name='DUMMY USER', dob='1970-01-01', weight=random.random()*100, height=random.random(), sex=random.choice(['h', 'l', 'm', 'k']))
        user = User(profile=profile)
        self.assertIsInstance(user, User)

    @secure_test
    def test_save_load_and_purge_user(self):
        name = 'DUMMY USER'
        profile = UserProfile(name=name, dob='1970-01-01', weight=random.random()*100, height=random.random(), sex=random.choice(['h', 'l', 'm', 'k']))

        user = User(profile=profile)
        filename = user.storage_file_basename

        self.assertFalse(user.saved)
        self.assertFalse(os.path.isfile(filename + '_profile.txt'))
        self.assertFalse(os.path.isfile(filename + '_dataframe.csv'))
        self.assertFalse(os.path.isfile(filename + '_dictionary.pickle'))

        user.save()
        self.assertTrue(user.saved)
        self.assertTrue(os.path.isfile(filename + '_profile.txt'))
        self.assertTrue(os.path.isfile(filename + '_dataframe.csv'))
        self.assertTrue(os.path.isfile(filename + '_dictionary.pickle'))

        loaded_user = User(name=name)
        self.assertIsInstance(loaded_user, User)

        user.purge()
        self.assertFalse(user.saved)
        self.assertFalse(os.path.isfile(filename + '_profile.txt'))
        self.assertFalse(os.path.isfile(filename + '_dataframe.csv'))
        self.assertFalse(os.path.isfile(filename + '_dictionary.pickle'))

class TestUsers(unittest.TestCase):

    def test_init_loaded_users(self):
        # Will load users if save is present
        users = Users()
        self.assertIsInstance(users, Users)

    def test_init_empty_users(self):
        users = Users(load=False)
        self.assertIsInstance(users, Users)
        self.assertEqual(len(users), 0)

    def test_init_add_user(self):
        name = 'DUMMY USER'
        profile = UserProfile(name=name, dob='1970-01-01', weight=random.random()*100, height=random.random(), sex=random.choice(['h', 'l', 'm', 'k']))
        user = User(profile=profile)

        users = Users(load=False)

        users.append(user)
        self.assertEqual(len(users), 1)
        self.assertEqual(name, users.user_list[0].profile.name)

    @secure_test
    def test_save_load_and_purge(self):
        name = 'DUMMY USER'
        profile = UserProfile(name=name, dob='1970-01-01', weight=random.random()*100, height=random.random(), sex=random.choice(['h', 'l', 'm', 'k']))
        user = User(profile=profile)
        users = Users(load=False)
        users.append(user)

        filename = user.storage_file_basename

        # nothing should be saved, and not save files for 'DUMMY USER'
        self.assertFalse(user.saved)
        self.assertFalse(users.saved)
        self.assertFalse(os.path.isfile(filename + '_profile.txt'))
        self.assertFalse(os.path.isfile(filename + '_dataframe.csv'))
        self.assertFalse(os.path.isfile(filename + '_dictionary.pickle'))

        users.save()
        # now everything should be saved, files created
        self.assertTrue(user.saved)
        self.assertTrue(users.saved)
        self.assertTrue(os.path.isfile(filename + '_profile.txt'))
        self.assertTrue(os.path.isfile(filename + '_dataframe.csv'))
        self.assertTrue(os.path.isfile(filename + '_dictionary.pickle'))

        users.remove(name)
        # now users should not be saved, and files still present
        self.assertTrue(user.saved)
        self.assertFalse(users.saved)
        self.assertTrue(os.path.isfile(filename + '_profile.txt'))
        self.assertTrue(os.path.isfile(filename + '_dataframe.csv'))
        self.assertTrue(os.path.isfile(filename + '_dictionary.pickle'))

        users.append(user)
        users.save()
        users.remove(name, purge=True)
        # now nothing should be saved, and files removed
        self.assertFalse(user.saved)
        self.assertFalse(users.saved)
        self.assertFalse(os.path.isfile(filename + '_profile.txt'))
        self.assertFalse(os.path.isfile(filename + '_dataframe.csv'))
        self.assertFalse(os.path.isfile(filename + '_dictionary.pickle'))



if __name__ == '__main__':
    pass
